<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['value','difficulty','category_id','is_active'];

    public function answers(){
        return $this->hasMany('App\QuestionAnswer');
    }

    public function category(){
        return $this->hasOne('App\Category','id', 'category_id');
    }

    public function getAnswersAttribute(){
        return $this->answers()->get();
    }

    public function getCategoryAttribute(){
        return $this->category()->first();
    }
}
