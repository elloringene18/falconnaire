<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['name','score','location_id','total_time'];

    public function location(){
        return $this->hasOne('App\Location','id','location_id');
    }

    public function members(){
        return $this->hasMany('App\TeamMember');
    }

    public function getLocationAttribute(){
        return $this->location()->first();
    }
}
