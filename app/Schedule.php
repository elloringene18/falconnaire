<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
//
//$table->bigInteger('team_a_id')->nullable();
//$table->bigInteger('team_b_id')->nullable();
//$table->dateTime('date_start')->nullable();
//$table->dateTime('date_end')->nullable();
//$table->dateTime('date_started')->nullable();
//$table->dateTime('date_ended')->nullable();
    //

    protected $fillable = ['team_a_id','team_b_id','date_start','date_end','date_started','date_ended'];

    public $dates = ['date_start','date_end','date_started','date_ended'];

    public function teama(){
        return $this->hasOne('App\Team','id', 'team_a_id');
    }

    public function teamb(){
        return $this->hasOne('App\Team','id','team_b_id');
    }
}
