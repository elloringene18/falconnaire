<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $fillable = ['team_a_id','team_b_id','location_id','date_start','date_end'];

    public $dates = ['date_start','date_end'];

    public function teama(){
        return $this->hasOne('App\Team','id', 'team_a_id');
    }

    public function teamb(){
        return $this->hasOne('App\Team','id','team_b_id');
    }

    public function location(){
        return $this->hasOne('App\Location','id', 'location_id');
    }

    public function getLocationAttribute(){
        return $this->location()->first();
    }

    public function getTeamAAttribute(){
        return $this->teama()->first();
    }

    public function getTeamBAttribute(){
        return $this->teamb()->first();
    }
}
