<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionAnswer extends Model
{
    protected $fillable = ['value','is_correct'];

    public function question(){
        return $this->hasOne('App\Question','id','question_id');
    }
}
