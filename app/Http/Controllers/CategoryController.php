<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = $this->model->get();
        return view('admin.categories.index', compact('data'));
    }

    public function create(){
        return view('admin.categories.create');
    }

    public function store(Request $request){
        $data = $request->input();

        if($this->model->where('name',$data['name'])->count()) {
            Session::flash('error', 'The name is already registered. Please use a different one');
            return redirect()->back();
        }

        $data = $this->model->create(['name' => $data['name']]);

        if($data){
            Session::flash('success','Record added successfully');
            return redirect()->back();
        }

        Session::flash('error','There was an error. Please try again.');

        return redirect()->back();
    }

    public function update(Request $request){
        $data = $request->input();

        $question = $this->model->find($data['id']);

        if($question){

            $question->update(['name' => $data['name']]);

            Session::flash('success','Record updated successfully');
            return redirect()->back();
        }

        Session::flash('error','There was an error. Please try again.');
        return redirect()->back();
    }

    public function edit($id){
        $item = $this->model->find($id);
        return view('admin.categories.edit', compact('item'));
    }

    public function delete($id){
        $item = $this->model->find($id);

        if($item)
            $item->delete();

        Session::flash('success','Item deleted successfully.');
        return redirect()->back();
    }
}
