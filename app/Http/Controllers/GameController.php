<?php

namespace App\Http\Controllers;

use App\Category;
use App\Location;
use App\Match;
use App\Question;
use App\Schedule;
use App\Setting;
use App\Team;
use Carbon\Carbon;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Session;

class GameController extends Controller
{
    public function index(){
//        $schedules = Schedule::whereDate('date_start','>=',Carbon::now())->
//        where('team_a_id','!=',null)->
//        where('team_b_id','!=',null)->
//        where('date_ended',null)->get();
//
        $schedules = Schedule::get();

        $settings['answer-duration'] = Setting::where('name','answer-duration')->first()->value;
        $settings['easy-points'] = Setting::where('name','easy-points')->first()->value;
        $settings['medium-points'] = Setting::where('name','medium-points')->first()->value;
        $settings['hard-points'] = Setting::where('name','hard-points')->first()->value;
        $settings['timer'] = Setting::where('name','timer')->first()->value;
        $settings['steal-timer'] = Setting::where('name','steal-timer')->first()->value;

        $categories = Category::get();

        return view('game', compact('schedules','settings','categories'));
    }

    public function getQuestionsByCat(Request $request){
        $data = [];

        $data['easy'] = Question::where('difficulty',0)->whereIn('category_id',$request->input('cats'))->inRandomOrder()->with('answers')->limit(4)->get()->toArray();
        $data['medium'] = Question::where('difficulty',1)->whereIn('category_id',$request->input('cats'))->inRandomOrder()->limit(4)->with('answers')->get()->toArray();
        $data['hard'] = Question::where('difficulty',2)->whereIn('category_id',$request->input('cats'))->inRandomOrder()->limit(4)->with('answers')->get()->toArray();

        return response()->json($data);
    }


    public function completeMatch(Request $request){
        $schedule = Schedule::find($request->input('schedule_id'));

        if($schedule){
            $schedule->date_ended = Carbon::now();
            $schedule->save();
            return response()->json(true);
        }

        return response()->json(false);
    }

    public function updateTeamScores(Request $request){
        $data = $request->input();

        $teamA = Team::find($data['team_a_id']);
        $teamB = Team::find($data['team_b_id']);

        if($teamA)
            $teamA->update(['score'=>$data['team_a_score'],'total_time'=>$data['team_a_time']]);
        if($teamB)
            $teamB->update(['score'=>$data['team_b_score'],'total_time'=>$data['team_b_time']]);

        $d['team_a'] = $teamA;
        $d['team_b'] = $teamB;

        return $data;
    }

    public function checkTeamMatch($id1,$id2){

        $a = Schedule::where('team_a_id',$id1)->orWhere('team_b_id',$id1)->first();

        if($a){
            if($a->date_ended != null)
                return response()->json(['data'=>false]);
        }

        $b = Schedule::where('team_a_id',$id2)->orWhere('team_b_id',$id2)->first();

        if($b){
            if($b->date_ended != null)
                return response()->json(['data'=>false]);
        }

        return response()->json(['data'=>true]);
    }

    public function scoreboard(){
		$rnk = [];

        $data = Team::orderBy('score','DESC')->limit(10)->get();

        foreach ($data as $index=>$team)
            $rnk[$team->score][$team->id] = $team->total_time;

        $teams = [];
        foreach ($rnk as $score=>$d){
            asort($d);
            foreach ($d as $id=>$v)
                $teams[] = Team::find($id);
        }

        return view('scoreboard',compact('teams'));
    }

    public function settingsUpdate(Request $request){
        $data = $request->input('settings');

        foreach ($data as $key=>$item){
            $setting = Setting::where('name',$key)->first();

            if($setting)
                $setting->update(['value'=>$item]);
        }

        Session::flash('success','Settings updated Successfully');
        return redirect()->back();
    }

    public function settings(){
        $data = Setting::get();

        $settings = [];

        foreach ($data as $setting)
            $settings[$setting->name] = $setting->value;

        return view('admin.settings',compact('settings'));
    }

}
