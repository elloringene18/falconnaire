<?php

namespace App\Http\Controllers;

use App\Location;
use App\Schedule;
use App\Team;
use App\TeamMember;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TeamController extends Controller
{
    public function __construct(Team $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = $this->model->with('members')->get();
        return view('admin.teams.index', compact('data'));
    }

    public function create(){
        $locations = Location::select('id','name')->get();
        return view('admin.teams.create',compact('locations'));
    }

    public function register(){
        $locations = Location::select('id','name')->get();
        $schedules = Schedule::where('team_b_id',null)->whereDate('date_start','>=',Carbon::now())->limit(4)->get();

        return view('register',compact('locations','schedules'));
    }

    public function store(Request $request){
        $data = $request->input();

        if($this->model->where('name',$data['name'])->count()){
            Session::flash('error','Team name is already taken.');
            $url = 'name='.$data['name'].'&location_id='.$data['location_id'];

            foreach ($data['members'] as $index=>$member)
                $url .= '&member_name'.$index.'='.urlencode($member['name']).'&member_id'.$index.'='.urlencode($member['id']);

            return redirect('admin/teams/create?'.$url);
        }

        $team = $this->model->create([
            'name' => $data['name'],
            'location_id'=>$data['location_id']
        ]);

        if($team){

            foreach ($data['members'] as $member){
                $team->members()->create([
                    'name' => $member['name'],
                    'fid' => $member['id'],
                ]);
            }

            Session::flash('success','Record added successfully');
            return redirect('/admin/teams');
        }

        Session::flash('error','There was an error. Please try again.');
        return redirect()->back();
    }

    public function registerTeam(Request $request){
        $data = $request->input();

        if($this->model->where('name',$data['name'])->count()){
            Session::flash('error','Team name is already taken.');
            $url = 'name='.$data['name'].'&location_id='.$data['location_id'];

            foreach ($data['members'] as $index=>$member)
                $url .= '&member_name'.$index.'='.urlencode($member['name']).'&member_id'.$index.'='.urlencode($member['id']);

            return redirect('register?'.$url);
        }

        $schedule = Schedule::find($data['schedule_id']);

        if($schedule->team_a_id != null && $schedule->team_b_id != null){
            Session::flash('error','Time slot has recently been taken. Please select a different one.');
            $url = 'name='.$data['name'].'&location_id='.$data['location_id'];

            foreach ($data['members'] as $index=>$member)
                $url .= '&member_name'.$index.'='.urlencode($member['name']).'&member_id'.$index.'='.urlencode($member['id']);

            return redirect('register?'.$url);
        }

        $ids = [];
        foreach ($data['members'] as $member){
            if($member['name']!=null && $member['id']!=null)
            {
                $ids[] = $member['id'];

                $existingMember = TeamMember::where('fid',$member['id'])->count();

                if($existingMember){
                    Session::flash('error','Member ID "'.$member['id'].'" is already registered.');

                    $url = 'name='.$data['name'].'&location_id='.$data['location_id'];

                    foreach ($data['members'] as $index=>$member)
                        $url .= '&member_name'.$index.'='.urlencode($member['name']).'&member_id'.$index.'='.urlencode($member['id']);

                    return redirect('register?'.$url);
                }

            }
        }

        if($this->has_dupes($ids)){
            Session::flash('error','Duplicate IDs detected. Please try again.');

            $url = 'name='.$data['name'].'&location_id='.$data['location_id'];

            foreach ($data['members'] as $index=>$member)
                $url .= '&member_name'.$index.'='.urlencode($member['name']).'&member_id'.$index.'='.urlencode($member['id']);

            return redirect('register?'.$url);
        }

        $team = $this->model->create([
            'name' => $data['name'],
            'location_id'=>$data['location_id']
        ]);

        if($team){

            $schedule = Schedule::where('id',$data['schedule_id'])->first();

            if($schedule->team_a_id == null)
                $schedule->team_a_id = $team->id;
            else if($schedule->team_b_id == null)
                $schedule->team_b_id = $team->id;

            $schedule->save();

            foreach ($data['members'] as $member){
                if($member['name']!=null && $member['id']!=null)
                    $team->members()->create([
                        'name' => $member['name'],
                        'fid' => $member['id']
                    ]);
            }

            Session::flash('success','You team has been registered successfully');
            return redirect('register');
        }

        Session::flash('error','There was an error. Please try again.');
        return redirect()->back();
    }

    private function has_dupes($array) {
        // streamline per @Felix
        return count($array) !== count(array_unique($array));
    }

    public function update(Request $request){
        $data = $request->input();

        $team = $this->model->find($data['id']);

        if($team){
            $team->update(['name'=>$data['name'],'location_id'=>$data['location_id']]);
            $team->members()->delete();

            foreach ($data['members'] as $member){
                if($member['name'] && $member['id'])
                    $team->members()->create([
                        'name' => $member['name'],
                        'fid' => $member['id'],
                    ]);
            }

            Session::flash('message','Record updated successfully');
            return redirect()->back();
        }

        Session::flash('message','There was an error. Please try again.');
        return redirect()->back();
    }

    public function edit($id){
        $locations = Location::select('id','name')->get();
        $item = $this->model->with('members')->find($id);
        return view('admin.teams.edit', compact('item','locations'));
    }

    public function getByLocation($location_id){
        $response['teams'] = Team::where('location_id',$location_id)->where('score',null)->get();
        return response()->json($response);
    }

    public function getBySchedule($schedule_id){
        $schedule = Schedule::where('id',$schedule_id)->first();

        $response['teams'][] = Team::where('id',$schedule->team_a_id)->first();
        $response['teams'][] = Team::where('id',$schedule->team_b_id)->first();

        return response()->json($response);
    }

    public function delete($id){
        $item = $this->model->find($id);

        if($item){
            $schedule = Schedule::where('team_a_id',$item->id)->first();

            if($schedule){
                $schedule->team_a_id = $schedule->team_b_id;
                $schedule->team_b_id = null;
                $schedule->save();
            }

            $schedule = Schedule::where('team_b_id',$item->id)->first();

            if($schedule){
                $schedule->team_b_id = null;
                $schedule->save();
            }

            $item->delete();
        }

        Session::flash('success','Item deleted successfully.');
        return redirect()->back();
    }
}
