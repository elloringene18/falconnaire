<?php

namespace App\Http\Controllers;

use App\Match;
use App\Schedule;
use App\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MatchController extends Controller
{
    public function __construct(Schedule $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = $this->model->get();
        return view('admin.matches.index', compact('data'));
    }

    public function edit($id){
        $schedule = Schedule::find($id);
        $teams = Team::get();

        if($schedule)
            return view('admin.matches.create', compact('schedule','teams'));

        return 'Match not found!';
    }

    public function update(Request $request){

        $schedule = Schedule::find($request->input('id'));

        $teama = $request->input('team_a_id');
        $teamb = $request->input('team_b_id');

        if($teama == $teamb && $teama != 'null' && $teamb != 'null'){
            Session::flash('error','Team A and B are the same team.');
            return redirect()->back();
        }

        if(Schedule::where('team_a_id',$teama)->orWhere('team_b_id', $teama)->count() && $schedule->team_a_id != $teama){
            Session::flash('error','Team A is already registered on a different match');
            return redirect()->back();
        }

        if(Schedule::where('team_a_id',$teamb)->orWhere('team_b_id', $teamb)->count() && $schedule->team_b_id != $teamb){
            Session::flash('error','Team B is already registered on a different match');
            return redirect()->back();
        }


        if($schedule){

            $schedule->team_a_id = ($teama == 'null' ? null : $teama);
            $schedule->team_b_id = ($teamb == 'null' ? null : $teamb);

            if($teama == 'null'){
                $schedule->team_a_id = ($teamb == 'null' ? null : $teamb);
                $schedule->team_b_id = null;
            }

            $schedule->save();

            Session::flash('success','Match Updated Successfully');
            return redirect()->back();
        }

        return 'Match not found!';
    }

    public function delete($id){
        $item = Schedule::where('id',$id)->first();

        if($item) {
            $teama = Team::find($item->team_a_id);
            $teamb = Team::find($item->team_b_id);

            if($teama)
                $teama->update(['score'=>null,'total_time'=>null]);
            if($teamb)
                $teamb->update(['score'=>null,'total_time'=>null]);

            $item->team_a_id = null;
            $item->team_b_id = null;
            $item->date_started = null;
            $item->date_ended = null;
            $item->save();
        }

        Session::flash('success','Item deleted successfully.');
        return redirect()->back();
    }
}
