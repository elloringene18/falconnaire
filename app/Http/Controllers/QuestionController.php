<?php

namespace App\Http\Controllers;

use App\Category;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class QuestionController extends Controller
{
    public function __construct(Question $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = $this->model->with('answers')->get();
        $categories = Category::get();

        return view('admin.questions.index', compact('data','categories'));
    }

    public function store(Request $request){
        $data = $request->input();

        $question = $this->model->create([
            'value' => $data['value'],
            'difficulty' => $data['difficulty'],
            'category_id' => $data['category_id']
        ]);

        if($question){
            foreach ($data['answers'] as $index=>$answer){
                if($answer){
                    $correct = $data['correct-answer'] == $index ? 1 : 0;
                    $question->answers()->create([
                        'value' => $answer,
                        'is_correct' => $correct
                    ]);
                }
            }

            Session::flash('success','Record added successfully');
            return redirect('/admin/questions');
        }

        Session::flash('message','There was an error. Please try again.');
        return redirect()->back();
    }

    public function update(Request $request){
        $data = $request->input();

        $question = $this->model->find($data['id']);

        if($question){

            $question->update([
                'value' => $data['value'],
                'difficulty' => $data['difficulty'],
                'category_id' => $data['category_id'],
            ]);

            $question->answers()->delete();

            foreach ($data['answers'] as $index=>$answer){
                if($answer){
                    $correct = $data['correct-answer'] == $index ? 1 : 0;
                    $question->answers()->create([
                        'value' => $answer,
                        'is_correct' => $correct
                    ]);
                }
            }

            Session::flash('message','Record updated successfully');
            return redirect()->back();
        }

        Session::flash('message','There was an error. Please try again.');
        return redirect()->back();
    }

    public function edit($id){
        $categories = Category::get();
        $item = $this->model->with('answers')->find($id);
        return view('admin.questions.edit', compact('item','categories'));
    }

    public function create(){
        $categories = Category::get();
        return view('admin.questions.create', compact('categories'));
    }

    public function delete($id){
        $item = $this->model->find($id);

        if($item)
            $item->delete();

        Session::flash('success','Item deleted successfully.');
        return redirect()->back();
    }
}
