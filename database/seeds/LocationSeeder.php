<?php

use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            [ 'name' => 'DXB Headquarters F1' ],
            [ 'name' => 'DXB Headquarters F2' ],
            [ 'name' => 'Sharjah Main F1' ],
            [ 'name' => 'Sharjah Main F2' ],
        ];

        foreach ($data as $index => $item){
            \App\Location::create($item);
        }
    }
}
