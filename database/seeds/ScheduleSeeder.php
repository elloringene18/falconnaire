<?php

use Illuminate\Database\Seeder;

class ScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $scheds = \App\Schedule::get();

        foreach ($scheds as $sched)
            $sched->delete();

        // $date = \Carbon\Carbon::parse('2022-04-15 09:00');
        $new['date_start'] = \Carbon\Carbon::parse('2022-04-14 09:00');
        $new['date_end'] = \Carbon\Carbon::parse('2022-05-15 09:00');

        // for ($y=0;$y<180;$y++){

        //     // if($date->format('D') != "Fri" && $date->format('D') != "Sat"){
        //         for ($x=0;$x<6;$x++){
        //             $new['date_start'] = \Carbon\Carbon::parse($date);
        //             $new['date_end'] = $date->addHours(1);

        //             // if(\App\Schedule::where('date_start',$new['date_start'])->where('date_end',$new['date_end'])->count()==0)
        //             \App\Schedule::create($new);
        //         }
        //     // }

        //     $date->addDay();
        //     $date = \Carbon\Carbon::parse($date->format('Y-m-d').' '. '9:00');
        // }

        \App\Schedule::create($new);

    }
}
