<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [ 'name' => 'Admin', 'email' => 'gene@thisishatch.com', 'password' => \Illuminate\Support\Facades\Hash::make('secret') ],
            [ 'name' => 'Admin', 'email' => 'admin@etisalat.ae', 'password' => \Illuminate\Support\Facades\Hash::make('ets2020!!!') ],
        ];


        foreach ($data as $index => $item){
            \App\User::create($item);
        }
    }
}



                                               