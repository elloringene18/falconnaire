<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [ 'name' => 'Data protection' ],
            [ 'name' => 'General Questions' ],
            [ 'name' => 'Malware' ],
            [ 'name' => 'Mobile Security' ],
            [ 'name' => 'Password Security' ],
            [ 'name' => 'Phishing' ],
            [ 'name' => 'Physical Security' ],
            [ 'name' => 'Social Enginnering' ],
            [ 'name' => 'Social Media' ],
        ];

        foreach ($data as $index => $item){
            $item['slug'] = \Illuminate\Support\Str::slug($item['name']);
            \App\Category::create($item);
        }
    }
}
