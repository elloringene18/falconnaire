<?php

use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [ 'name' => 'Team A', 'location_id' => 1 ],
            [ 'name' => 'Team B', 'location_id' => 1 ],
            [ 'name' => 'Team C', 'location_id' => 1 ],
            [ 'name' => 'Team D', 'location_id' => 1 ],
            [ 'name' => 'Team E', 'location_id' => 2 ],
            [ 'name' => 'Team F', 'location_id' => 2 ],
            [ 'name' => 'Team G', 'location_id' => 2 ],
            [ 'name' => 'Team H', 'location_id' => 2 ],
            [ 'name' => 'Team I', 'location_id' => 3 ],
            [ 'name' => 'Team J', 'location_id' => 3 ],
            [ 'name' => 'Team K', 'location_id' => 3 ],
            [ 'name' => 'Team L', 'location_id' => 3 ],
            [ 'name' => 'Team M', 'location_id' => 4 ],
            [ 'name' => 'Team N', 'location_id' => 4 ],
            [ 'name' => 'Team O', 'location_id' => 4 ],
            [ 'name' => 'Team P', 'location_id' => 4 ],
        ];

        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        foreach ($data as $index => $item){
            $newTeam = \App\Team::create($item);

            for($x=0;$x<4;$x++){
                $newTeam->members()->create(['name'=>substr(str_shuffle(str_repeat($pool, 5)), 0, 8),'fid'=>rand(100000,999999)]);
            }
        }
    }
}
