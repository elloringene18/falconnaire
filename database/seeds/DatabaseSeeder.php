<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategorySeeder::class);
        $this->call(QuestionSeeder::class);
        $this->call(LocationSeeder::class);
        /*** $this->call(TeamSeeder::class); **/
        $this->call(UserSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(ScheduleSeeder::class);
    }
}
