<?php

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [ 'name' => 'answer-duration', 'value' => '1000' ],
            [ 'name' => 'easy-points', 'value' => '5' ],
            [ 'name' => 'medium-points', 'value' => '10' ],
            [ 'name' => 'hard-points', 'value' => '15' ],
            [ 'name' => 'timer', 'value' => '30' ],
            [ 'name' => 'steal-timer', 'value' => '15' ],
        ];

        foreach ($data as $index => $item){
            \App\Setting::create($item);
        }
    }
}
