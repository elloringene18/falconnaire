<?php

use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
//            [
//                'value' => 'What are the three attributes of information security?',
//                'difficulty' => 0,
//                'answers' => [
//                    [ 'value' => 'Confidential, Information, Available (CIA)' ],
//                    [ 'value' => 'Safety, Protection, Prevention (SPP)' ],
//                    [ 'value' => 'Safety, Protection, Prevention (SPP)' ],
//                    [ 'value' => 'Confidentiality, Integrity, Availability (CIA)', 'is_correct' => 1 ],
//                ],
//                'category_id' => 1
//            ],
//            [
//
//                'value' => 'When data is changed by an unauthorized person, that is a breach of?  ',
//                'difficulty' => 0,
//                'answers' => [
//                    [ 'value' => 'Confidentiality' ],
//                    [ 'value' => 'Integrity', 'is_correct' => 1 ],
//                    [ 'value' => 'Availability' ],
//                    [ 'value' => 'All of the above' ],
//                ]
//            ],
//            [
//
//                'value' => 'What is dumpster diving?',
//                'difficulty' => 0,
//                'answers' => [
//                    [ 'value' => 'Throwing confidential information in recycle bins' ],
//                    [ 'value' => 'Searching an organization’s trash for confidential or personal data', 'is_correct' => 1 ],
//                    [ 'value' => 'Throwing confidential information in the trash' ],
//                ]
//            ],
//            [
//                'value' => 'Which of the following is NOT an accepted behavior in Etisalat?',
//                'difficulty' => 0,
//                'answers' => [
//                    [ 'value' => 'Allowing visitors or vendors to enter behind you and bypass physical security controls', 'is_correct' => 1  ],
//                    [ 'value' => 'Questioning strangers roaming around in the Etisalat building' ],
//                    [ 'value' => 'Requesting visitors to register at the reception before entering the building' ],
//                    [ 'value' => 'All of the above are not accepted' ],
//                ]
//            ],
//            [
//                'value' => 'What should you do if you see a visitor roaming around in a restricted area?',
//                'difficulty' => 0,
//                'answers' => [
//                    [ 'value' => 'If the visitor is not lost, then nothing' ],
//                    [ 'value' => 'Question the visitor politely and escort them to the meeting room', 'is_correct' => 1 ],
//                    [ 'value' => 'Request the visitor to show you a valid ID' ],
//                    [ 'value' => 'Request the visitor to leave the building immediately ' ],
//                ]
//            ],
//            [
//                'value' => 'What is malware?',
//                'difficulty' => 0,
//                'answers' => [
//                    [ 'value' => 'Any malicious software infecting devices ', 'is_correct' => 1 ],
//                    [ 'value' => 'A free software for enhancing security' ],
//                    [ 'value' => 'A software for managing g hardware' ],
//                ]
//            ],
//            [
//                'value' => 'If a device is infected with a malicious code that encrypts or locks files and requests money to return or decrypt the files, the device is infected with?',
//                'difficulty' => 0,
//                'answers' => [
//                    [ 'value' => 'A virus' ],
//                    [ 'value' => 'A worm' ],
//                    [ 'value' => 'A ransomware', 'is_correct' => 1 ],
//                    [ 'value' => 'A trojan' ],
//                ]
//            ],
//            [
//                'value' => 'What should you do if you receive a suspicious SMS message that contains a link? ',
//                'difficulty' => 0,
//                'answers' => [
//                    [ 'value' => 'Delete the message', 'is_correct' => 1 ],
//                    [ 'value' => 'Click on the link to view the website because it is safe to click on links received via SMS ' ],
//                    [ 'value' => 'Use an antivirus to scan the link before clicking' ],
//                    [ 'value' => 'Forward the message to all your friends to tell them about the scam ' ],
//                ]
//            ],
//            [
//                'value' => 'What should you do if you receive a call from a recruitment agency asking you for detail information about your project and your current employer’s internal processes? ',
//                'difficulty' => 0,
//                'answers' => [
//                    [ 'value' => 'Verify the identity of the caller and provide the information' ],
//                    [ 'value' => 'Never provide any information about the organization to any recruitment agency ', 'is_correct' => 1 ],
//                    [ 'value' => 'Only provide information about the staff and projects' ],
//                    [ 'value' => 'Provide personal information immediately as it might be a great job opportunity' ],
//                ]
//            ],
//            [
//                'value' => 'How can you verify if an email is phishing or real?',
//                'difficulty' => 1,
//                'answers' => [
//                    [ 'value' => 'Check the email address' ],
//                    [ 'value' => 'Hover over the link to verify the address of the URL' ],
//                    [ 'value' => 'Pay attention to the content of the email and sense of urgency'],
//                    [ 'value' => 'All of the above', 'is_correct' => 1  ],
//                ]
//            ],
//            [
//                'value' => 'How can you verify if a website is secure before entering credentials or credit card number? ',
//                'difficulty' => 1,
//                'answers' => [
//                    [ 'value' => 'Check the branding and logos used on the website ' ],
//                    [ 'value' => 'Verify that the URL starts with https:// and check for the lock as well as the trusted certificate ', 'is_correct' => 1 ],
//                    [ 'value' => 'Pay attention to any spelling mistakes' ],
//                    [ 'value' => 'All of the above' ],
//                ]
//            ],
//            [
//                'value' => 'Which of the below describes a strong password? ',
//                'difficulty' => 1,
//                'answers' => [
//                    [ 'value' => 'Contains a foreign language word '],
//                    [ 'value' => 'Is at least 12 characters long and contains numbers, small & capital letter characters and special characters ', 'is_correct' => 1  ],
//                    [ 'value' => 'Is a long phrase with multiple words' ],
//                    [ 'value' => 'A difficult word from the dictionary' ],
//                ]
//            ],
//            [
//                'value' => 'Which of the below is a password security bets practice? ',
//                'difficulty' => 1,
//                'answers' => [
//                    [ 'value' => 'Using default passwords '],
//                    [ 'value' => 'Writing down passwords to remember them ' ],
//                    [ 'value' => 'Changing passwords regularly ', 'is_correct' => 1  ],
//                    [ 'value' => 'Using the same password for all accounts' ],
//                ]
//            ],
//            [
//                'value' => 'What are the classification levels used in Etisalat? ',
//                'difficulty' => 1,
//                'answers' => [
//                    [ 'value' => 'Public, internal, confidential, secret '],
//                    [ 'value' => 'Public, internal, confidential, strictly confidential ' ],
//                    [ 'value' => 'Public, internal, confidential, restricted', 'is_correct' => 1  ],
//                    [ 'value' => 'Public, internal, secret, top secret ' ],
//                ]
//            ],
//            [
//                'value' => 'What is the classification of any financial data in Etisalat? ',
//                'difficulty' => 1,
//                'answers' => [
//                    [ 'value' => 'Public'],
//                    [ 'value' => 'Internal' ],
//                    [ 'value' => 'Restricted', 'is_correct' => 1  ],
//                    [ 'value' => 'Secret ' ],
//                ]
//            ],
//            [
//                'value' => 'What is clear desk policy? ',
//                'difficulty' => 1,
//                'answers' => [
//                    [ 'value' => 'Cleaning your desk and removing all trash  '],
//                    [ 'value' => 'Storing confidential documents, media and mobile devices in locked cabinets whenever leaving your desk ', 'is_correct' => 1  ],
//                    [ 'value' => 'Removing all personal pictures and storing them in a locked cabinet  ' ],
//                ]
//            ],
//            [
//                'value' => 'Since the screen licks automictically, should you lock your screen?  ',
//                'difficulty' => 2,
//                'answers' => [
//                    [ 'value' => 'Yes, the attacker only needs a few seconds to get access if the machine is not locked immediately ', 'is_correct' => 1  ],
//                    [ 'value' => 'No, the control has been implemented by the security team and users are not required to lock their screen in Etisalat ' ],
//                ]
//            ],
//            [
//                'value' => 'What is the best way t dispose of information on your mobile devices?',
//                'difficulty' => 2,
//                'answers' => [
//                    [ 'value' => 'Break the phone'],
//                    [ 'value' => 'Store all data on the cloud and delete the data ' ],
//                    [ 'value' => 'Securely wipe all data on your mobile device ', 'is_correct' => 1  ],
//                    [ 'value' => 'Delete all data ' ],
//                ]
//            ],
//            [
//                'value' => 'What information security practice shall you apply when installing mobile apps? ',
//                'difficulty' => 2,
//                'answers' => [
//                    [ 'value' => 'Never adding your credit card to the App Store of Play Store '],
//                    [ 'value' => 'Verify the app permissions', 'is_correct' => 1  ],
//                    [ 'value' => 'Ask your friends about the app ' ],
//                    [ 'value' => 'None of the above  ' ],
//                ]
//            ],
//            [
//                'value' => 'Which of the below is a correct statement? ',
//                'difficulty' => 2,
//                'answers' => [
//                    [ 'value' => 'The operating system of the phone shall always be updated to ensure installing the latest security fixes ', 'is_correct' => 1  ],
//                    [ 'value' => 'Updating the operating system if the phone is not recommended as it always makes the phone slow'],
//                    [ 'value' => 'Updating the operating system if the phone is not recommended as some apps stop working ' ],
//                    [ 'value' => 'Updates for the operating system of the phone shall be done once a year ' ],
//                ]
//            ],
//            [
//                'value' => 'Which of the below is NOT acceptable in Etisalat? ',
//                'difficulty' => 2,
//                'answers' => [
//                    [ 'value' => 'Posting information about your systems used in Etisalat on LinkedIn' ],
//                    [ 'value' => 'Commenting on behalf of Etisalat on social media '],
//                    [ 'value' => 'Adding your colleagues in Etisalat on social media' ],
//                    [ 'value' => 'Both A & B ', 'is_correct' => 1  ],
//                ]
//            ],
//            [
//                'value' => 'Which statement is correct? ',
//                'difficulty' => 2,
//                'answers' => [
//                    [ 'value' => 'Default social media privacy settings are good to ensure information protection '  ],
//                    [ 'value' => 'Privacy settings are not needed '],
//                    [ 'value' => 'Privacy settings have to be configured to endure having the suitable and desired level of privacy', 'is_correct' => 1 ],
//                    [ 'value' => 'Changing default privacy settings might expose your personal data ' ],
//                ]
//            ],



//        1 [ 'name' => 'Data protection' ],
//        2 [ 'name' => 'General Questions' ],
//        3 [ 'name' => 'Malware' ],
//        4 [ 'name' => 'Mobile Security' ],
//        5 [ 'name' => 'Password Security' ],
//        6 [ 'name' => 'Phishing' ],
//        7 [ 'name' => 'Physical Security' ],
//        8 [ 'name' => 'Social Enginnering' ],
//        9 [ 'name' => 'Social Media' ],


            //
            // /////////////////////////////////// DATA PROTECTION
            //

            [
                'value' => 'Etisalat’s data classifications are:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Public, Internal, Secret, Restricted' ],
                    [ 'value' => 'Public, Internal, Confidential, Restricted', 'is_correct' => 1 ],
                    [ 'value' => 'External, Internal, Confidential, Restricted'],
                    [ 'value' => 'Public, Internal, Confidential, Top Secret' ],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Public (everyone can see the information): Can be disclosed to anyone without violating rights to privacy. ',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'True', 'is_correct' => 1 ],
                    [ 'value' => 'False'],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Internal (Data Classificaion): Intended for use only within the corporation. Unauthorized access could lead to loss of privacy. ',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'True', 'is_correct' => 1 ],
                    [ 'value' => 'False'],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Information classification may change over time',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'False' ],
                    [ 'value' => 'True', 'is_correct' => 1],
                    [ 'value' => 'Only Confidential information'],
                    [ 'value' => 'Only Confidential and Restricted information'],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Protection of staff information is the responsibility of ',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'HR Team' ],
                    [ 'value' => 'Finance Team'],
                    [ 'value' => 'All Staff', 'is_correct' => 1],
                    [ 'value' => 'Security Team'],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Staff email addresses are',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Public'],
                    [ 'value' => 'Internal', 'is_correct' => 1 ],
                    [ 'value' => 'Confidential'],
                    [ 'value' => 'Restricted'],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'A hard disk is an information asset of Etisalat:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'False'],
                    [ 'value' => 'Only when it contains information  that is owned by Etisalat', 'is_correct' => 1 ],
                    [ 'value' => 'Only when attached to a PC'],
                    [ 'value' => 'Only when encrypted'],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Staff personal record is:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Confidential', 'is_correct' => 1 ],
                    [ 'value' => 'Internal'],
                    [ 'value' => 'Restricted'],
                    [ 'value' => 'Public'],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Important considerations for data storage include:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Encryption'],
                    [ 'value' => 'Physical security'],
                    [ 'value' => 'Access permissions'],
                    [ 'value' => 'All of these', 'is_correct' => 1 ],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Software source code is an information asset of Etisalat',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'True', 'is_correct' => 1 ],
                    [ 'value' => 'False'],
                    [ 'value' => 'Only a software developed by Etisalat'],
                    [ 'value' => 'Only a software developed by a vendor'],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Confidential (medium confidentiality level): Information that the corporation and its employees have legal regulatory or social obligation to protect. It is sensitive within the organization. Unauthorized disclosure, compromise, or destruction would have significant or severe impact on the corporation or its employees. ',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'True', 'is_correct' => 1 ],
                    [ 'value' => 'False'],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Restricted (top confidentiality level): Information that is extremely sensitive and could cause extreme damage to image or effective service delivery of the organization. Restricted information is available only to named individuals or specified positions.',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'False'],
                    [ 'value' => 'True', 'is_correct' => 1 ],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'A hard disk is an information asset of Etisalat:',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'False'],
                    [ 'value' => 'Only if it contains Etisalat owned information.', 'is_correct' => 1 ],
                    [ 'value' => 'Only when attached to a PC' ],
                    [ 'value' => 'Only when encrypted'],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Encryption is:',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'A mechanism to transform readable data into an unreadable format', 'is_correct' => 1],
                    [ 'value' => 'A mechanism to reorganize text into an unreadable format'],
                    [ 'value' => 'A mechanism to transform unreadable format to readable format'],
                    [ 'value' => 'A mechanism to trigger text reorganization in data protection' ],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Strategic business plans are:',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Internal'],
                    [ 'value' => 'Confidential'],
                    [ 'value' => 'Restricted', 'is_correct' => 1 ],
                    [ 'value' => 'Internal'],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'What is the purpose of a data classification?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'To divide data into the categories of government-controlled and privately-controlled'],
                    [ 'value' => 'To identify what data is exclusively the responsibility of upper management'],
                    [ 'value' => 'To identify what data needs to be protected', 'is_correct' => 1],
                    [ 'value' => 'To identify what data can be discarded' ],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Information that the corporation and its employees have legal regulatory or social obligation to protect. It is sensitive within the organization. Unauthorized disclosure, compromise, or destruction would have significant or severe impact on the corporation or its employees. This information should be classified as -',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Confidential (medium confidentiality level) ', 'is_correct' => 1],
                    [ 'value' => 'Restricted (top confidentiality level)'],
                    [ 'value' => 'Public'],
                    [ 'value' => 'Internal (lowest level of confidentiality)' ],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Information that is extremely sensitive and could cause extreme damage to image or effective service delivery of the organization. This information is available only to named individuals or specified positions. This information should be classified as -',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Internal (lowest level of confidentiality)'],
                    [ 'value' => 'Restricted (top confidentiality level)', 'is_correct' => 1],
                    [ 'value' => 'Public'],
                    [ 'value' => 'Confidential (medium confidentiality level)' ],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Marketing brochures, Customer disclosure statements, published annual reports, Interviews with news media and Press releases are examples of which classification of data.',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Internal'],
                    [ 'value' => 'Restricted'],
                    [ 'value' => 'Public', 'is_correct' => 1 ],
                    [ 'value' => 'Confidential'],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Employee handbook, Telephone directories, organizational charts, policies, procedures, standards, internal reports and statistics are examples of which classification of data.',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Internal', 'is_correct' => 1],
                    [ 'value' => 'Restricted'],
                    [ 'value' => 'Public'],
                    [ 'value' => 'Confidential' ],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Staff personnel records, Customer records, Business plans, budget information and network diagrams are examples of which classification of data.',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Internal'],
                    [ 'value' => 'Restricted'],
                    [ 'value' => 'Public'],
                    [ 'value' => 'Confidential', 'is_correct' => 1 ],
                ],
                'category_id' => 1
            ],
            [
                'value' => 'Strategic plans, access codes such as passwords or PINs, credit card listings, Encryption keys, tender information and financial data are examples of which classification of data.',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Internal'],
                    [ 'value' => 'Restricted', 'is_correct' => 1 ],
                    [ 'value' => 'Public'],
                    [ 'value' => 'Confidential'],
                ],
                'category_id' => 1
            ],

            //
            // /////////////////////////////////// GENERAL QUESTIONS
            //

            [
                'value' => 'Which of the following would be dangerous to view on a public computer?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Sports scores' ],
                    [ 'value' => 'Weather report'],
                    [ 'value' => 'Flight times'],
                    [ 'value' => 'Any confidential information', 'is_correct' => 1],
                ],
                'category_id' => 2
            ],
            [
                'value' => 'Who is responsible for protecting Etisalat’s information?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'My supervisor' ],
                    [ 'value' => 'HR department'],
                    [ 'value' => 'Each Staff member', 'is_correct' => 1 ],
                    [ 'value' => 'IT department' ],
                ],
                'category_id' => 2
            ],
            [
                'value' => 'Why should caution be used when printing documents?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Printed documents can be destroyed' ],
                    [ 'value' => 'Papers are expensive ' ],
                    [ 'value' => 'Sensitive documents can be left in the open for anyone to read', 'is_correct' => 1],
                    [ 'value' => 'The printer could malfunction destroying the associated data' ],
                ],
                'category_id' => 2
            ],
            [
                'value' => 'You receive an email with the subject line "Urgent Attention required regarding your bank account!" you should:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Contact your bank directly to confirm.', 'is_correct' => 1 ],
                    [ 'value' => 'Open the email and look to confirm that it is from your bank.' ],
                    [ 'value' => 'Click the link in the email.'],
                    [ 'value' => 'Reply to the email with your personal telephone number.' ],
                ],
                'category_id' => 2
            ],
            [
                'value' => 'Malware can be found in Microsoft Office documents.',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'True', 'is_correct' => 1 ],
                    [ 'value' => 'False'],
                ],
                'category_id' => 2
            ],
            [
                'value' => 'Which of the following cannot be a source of viruses?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Websites' ],
                    [ 'value' => 'Downloads' ],
                    [ 'value' => 'Electronic documents'],
                    [ 'value' => 'They are all potential sources of malware', 'is_correct' => 1 ],
                ],
                'category_id' => 2
            ],
            [
                'value' => 'Using unlicensed or stolen software is:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Software piracy', 'is_correct' => 1 ],
                    [ 'value' => 'Easy' ],
                    [ 'value' => 'Risk-free'],
                    [ 'value' => 'Software privacy' ],
                ],
                'category_id' => 2
            ],
            [
                'value' => 'Always be wary of URLs that contain:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'All of these', 'is_correct' => 1 ],
                    [ 'value' => 'Numbers' ],
                    [ 'value' => 'Misspellings'],
                    [ 'value' => 'Tiny urls' ],
                ],
                'category_id' => 2
            ],
            [
                'value' => 'When looking at a URL, the hostname is where?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Right after http:// or https://', 'is_correct' => 1 ],
                    [ 'value' => 'At the end of the URL' ],
                    [ 'value' => 'After the .com'],
                    [ 'value' => 'At the beginning' ],
                ],
                'category_id' => 2
            ],
            [
                'value' => 'The visible link tells you ',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Where the link actually goes'],
                    [ 'value' => 'Where the link is supposed to go', 'is_correct' => 1  ],
                ],
                'category_id' => 2
            ],
            [
                'value' => 'A URL tells you ',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Where the link actually goes', 'is_correct' => 1  ],
                    [ 'value' => 'Where the link is supposed to go'],
                ],
                'category_id' => 2
            ],
            [
                'value' => 'There are two parts to every link: the _______ and the _________.',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Visible link, URL', 'is_correct' => 1 ],
                    [ 'value' => 'Visible link, USB' ],
                    [ 'value' => 'Visible URL, link'],
                    [ 'value' => 'SSH, TLS' ],
                ],
                'category_id' => 2
            ],
            [
                'value' => 'How could a hacker use the WiFi name (SSID) to trick you?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'By creating, similar named WIFI to a nearby legitimate WIFI network', 'is_correct' => 1 ],
                    [ 'value' => 'Hackers would not use this technique ' ],
                    [ 'value' => 'WiFi networks are generally not named'],
                    [ 'value' => 'To inject code into your computer' ],
                ],
                'category_id' => 2
            ],
            [
                'value' => 'What does the lock icon indicate when selecting a WiFi network?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'That the router uses HTTPS' ],
                    [ 'value' => 'That the network is full' ],
                    [ 'value' => 'A password is required to join', 'is_correct' => 1],
                    [ 'value' => 'That the router is up to date' ],
                ],
                'category_id' => 2
            ],
            [
                'value' => 'What of the following, should you pay special attention to when picking a WiFi network?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'The lock icon' ],
                    [ 'value' => 'The WiFi name field (SSID)' ],
                    [ 'value' => 'Both of these', 'is_correct' => 1],
                    [ 'value' => 'Neither of these' ],
                ],
                'category_id' => 2
            ],
            [
                'value' => 'What piece of information is important due to security when selecting a WiFi network?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Password icon and SSID', 'is_correct' => 1 ],
                    [ 'value' => 'The length of the antenna' ],
                    [ 'value' => 'That you can connect automatically'],
                    [ 'value' => 'Signal icon uses radar shaped bars' ],
                ],
                'category_id' => 2
            ],
            [
                'value' => 'What is a breach?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Unauthorized use or disclosure of Etisalat Customer Information ', 'is_correct' => 1  ],
                    [ 'value' => 'A hole in the wall of the server room'],
                    [ 'value' => 'A blind spot in the security system of the building'],
                    [ 'value' => 'Copying information from Etisalat’s public website' ],
                ],
                'category_id' => 2
            ],

            //
            // /////////////////////////////////// Malware
            //

            [
                'value' => 'A virus can damage which of the following?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Computers' ],
                    [ 'value' => 'Network-connected devices'],
                    [ 'value' => 'Cell phones'],
                    [ 'value' => 'All of these', 'is_correct' => 1],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'Opening files is safe when:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'They come from a trusted source', 'is_correct' => 1 ],
                    [ 'value' => 'Always'],
                    [ 'value' => 'Never'],
                    [ 'value' => 'The filename is not suspicious'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'Turning on operating system automatic updates:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Is unsafe and could result in a malware ' ],
                    [ 'value' => 'Only necessary for official devices'],
                    [ 'value' => 'Is generally a good practice', 'is_correct' => 1],
                    [ 'value' => 'Should never be done'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'A good protection against malware is to:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Never open files' ],
                    [ 'value' => 'Never install software'],
                    [ 'value' => 'Never use instant message services'],
                    [ 'value' => 'Never open files from people you do not know', 'is_correct' => 1],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'Malware can be distributed through:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Flash drives' ],
                    [ 'value' => 'All of these', 'is_correct' => 1],
                    [ 'value' => 'Email attachments'],
                    [ 'value' => 'Browser plugins'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'A good protection against malware is to:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Never open emails' ],
                    [ 'value' => 'Use a public computer to open flash drives'],
                    [ 'value' => 'Never open files that arrive unexpectedly', 'is_correct' => 1],
                    [ 'value' => 'Never watch Netflix at work'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'Pick the answer that is true of antivirus software:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Offers 100% protection against malware' ],
                    [ 'value' => 'Is not needed on personal devices'],
                    [ 'value' => 'Is not available for Microsoft products'],
                    [ 'value' => 'Scans for malware in files before opening', 'is_correct' => 1],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'What is generally true about downloading and opening files in emails?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Only to be done in emergencies' ],
                    [ 'value' => 'Need to be treated with suspicion and caution', 'is_correct' => 1],
                    [ 'value' => 'Is safe assuming your email client is updated'],
                    [ 'value' => 'Is safe assuming your antivirus is updated'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'Which of the following is a good strategy for avoiding malware?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Operating system updates' ],
                    [ 'value' => 'Downloading only from trusted sources'],
                    [ 'value' => 'Antivirus'],
                    [ 'value' => 'All of these', 'is_correct' => 1],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'You are visiting your favorite website and it warns you that your Flash plugin is out of date. You should',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Ignore all updates ' ],
                    [ 'value' => 'Install the update using only 4G data from mobile'],
                    [ 'value' => 'Only download the update from the official source', 'is_correct' => 1],
                    [ 'value' => 'Install the update from your favorite website'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'You found a flash drive and want to return it to the owner. You wonder if information on the drive could help you find the owner. You should:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Look for owner information by inserting it.' ],
                    [ 'value' => 'Drop it into a mailbox'],
                    [ 'value' => 'Put it in a drawer until you update antivirus'],
                    [ 'value' => 'Ask IT (888) for help', 'is_correct' => 1],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'You read from an official source that your software has a new critical update. You should:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Ignore the update' ],
                    [ 'value' => 'Update from a third party source'],
                    [ 'value' => 'Never install updates'],
                    [ 'value' => 'Update & turn on auto updates', 'is_correct' => 1],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'You receive an email from your coworker (Ahmed). You and Ahmed are working on an important presentation. Ahmed sends you a PowerPoint presentation for review. You should:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'View the file on Ahmed\'s computer' ],
                    [ 'value' => 'It is safe to download and open', 'is_correct' => 1],
                    [ 'value' => 'Ask Ahmed to print out the presentation.'],
                    [ 'value' => 'Let Ahmed give the presentation'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'You want to make sure that your software always has the latest patches to avoid malware. One strategy you could employ is:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Turn on automatic updates', 'is_correct' => 1 ],
                    [ 'value' => 'Use public computers'],
                    [ 'value' => 'Buy new computer frequently'],
                    [ 'value' => 'Install updates whenever asked'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'What popular operating system has compatible antivirus software?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'All of these', 'is_correct' => 1 ],
                    [ 'value' => 'Windows'],
                    [ 'value' => 'Macintosh'],
                    [ 'value' => 'Linux'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'A good protection against malware is to:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Using public WiFi' ],
                    [ 'value' => 'Never open files that look unusual', 'is_correct' => 1],
                    [ 'value' => 'Open all files but don\'t click install'],
                    [ 'value' => 'Turn off automatic updates'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'Why is it dangerous to use removable media from an untrusted source?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'If you have antivirus installed it is safe to plug into your computer' ],
                    [ 'value' => 'It could be a ploy by a hacker to get you to plug it in and transfer malware to your computer', 'is_correct' => 1],
                    [ 'value' => 'It is customary to look for files on the drive to help you identify the owner and return it to the owner'],
                    [ 'value' => 'It is best to plug it into a public computer'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'Your boss gives you a flash drive. She explains that the drive contains all the necessary files to complete your project. It is an official Etisalat USB drive, what should you do?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Use the flash drive to use the files.', 'is_correct' => 1 ],
                    [ 'value' => 'Ask your boss to email you the files instead'],
                    [ 'value' => 'Report the behavior to your IT department'],
                    [ 'value' => 'Format the flash drive to make it safe'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'Small, low-risk updates that fix software defects are called:',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Bugs' ],
                    [ 'value' => 'Patches', 'is_correct' => 1],
                    [ 'value' => 'Automatic emails'],
                    [ 'value' => 'Upgrades'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'What is one limitation of antivirus?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Is very expensive' ],
                    [ 'value' => 'Not available for every device', 'is_correct' => 1],
                    [ 'value' => 'Not available for mobile devices'],
                    [ 'value' => 'Only available for Windows PCs'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'Which of the following facts is TRUE about malware?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Dumb hardware, like a toaster, is most effected' ],
                    [ 'value' => 'Smartphones are not targeted by malware'],
                    [ 'value' => 'Is a malicious software that is harmful to a computer', 'is_correct' => 1],
                    [ 'value' => 'Malware is only a problem on Microsoft Windows'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'Which of the following is TRUE of antivirus software?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Is not available for mobile devices' ],
                    [ 'value' => 'Is often used on smart devices'],
                    [ 'value' => 'Only block malware that it understands or recognizes', 'is_correct' => 1],
                    [ 'value' => 'Prevents all access to the computer'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'The following is true of antivirus except:',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Makes opening all files safe', 'is_correct' => 1],
                    [ 'value' => 'Checks files before they are opened or run'],
                    [ 'value' => 'Only detects malware that it understands'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'You want to make sure that your software always has the latest patches to avoid malware. One strategy you could employ is:',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Turn on automatic updates', 'is_correct' => 1 ],
                    [ 'value' => 'Use public computers'],
                    [ 'value' => 'Buy new computer frequently'],
                    [ 'value' => 'Install updates whenever asked'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'Your Web browser is constantly being redirected to sites you didn’t want to visit and you’re seeing a lot of strange ads pop up. What’s the most likely culprit?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Browser hijacker', 'is_correct' => 1 ],
                    [ 'value' => 'Adware'],
                    [ 'value' => 'Adblocker'],
                    [ 'value' => 'Ransomware'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'You are visiting your favorite website and it warns you that your Flash plugin is out of date. You should:',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Ignore updates on your computer' ],
                    [ 'value' => 'Install the update using a private browsing session'],
                    [ 'value' => 'Only download the update from the official source.', 'is_correct' => 1],
                    [ 'value' => 'Install the update from your favorite website'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'Which of the following is NOT true about malware?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Malware is dangerous software' ],
                    [ 'value' => 'Malware isn\'t a concern on mobile devices', 'is_correct' => 1],
                    [ 'value' => 'Malware is short for "malicious software"'],
                    [ 'value' => 'Some forms of malware are often called viruses'],
                ],
                'category_id' => 3
            ],
            [
                'value' => 'Which of the following facts is TRUE about malware?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Dumb hardware, like a toaster, is most vulnerable' ],
                    [ 'value' => 'Smartphones are not targeted by malware'],
                    [ 'value' => 'Malware often exploits old security defects', 'is_correct' => 1],
                    [ 'value' => 'Malware is only a problem on Microsoft Windows'],
                ],
                'category_id' => 3
            ],

            //
            // /////////////////////////////////// MOBILE SECURITY
            //
            [
                'value' => 'One protection against data theft in case of a lost or stolen mobile device is:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'A privacy phone screen' ],
                    [ 'value' => 'Automatic Screen Lock', 'is_correct' => 1],
                    [ 'value' => 'Turning WiFi off'],
                    [ 'value' => 'Turning mobile hotspot off'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'One way to keep data safe from even stubborn thieves is:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'A protective phone case' ],
                    [ 'value' => 'Turning off Bluetooth'],
                    [ 'value' => 'Data Encryption', 'is_correct' => 1],
                    [ 'value' => 'Using another language'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'What statement is most true about antivirus software on mobile devices?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Lowers the security posture of your device' ],
                    [ 'value' => 'Is available and a good security practice', 'is_correct' => 1],
                    [ 'value' => 'Does not exist'],
                    [ 'value' => 'Is almost always a rogue app'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'What statement is most true about antivirus software on mobile devices?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Does not exist' ],
                    [ 'value' => 'Is almost always a rogue app'],
                    [ 'value' => 'Is generally available & a good security practice ', 'is_correct' => 1],
                    [ 'value' => 'Lowers the security posture of devices'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'Software and firmware on mobile devices should:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Installed once a month' ],
                    [ 'value' => 'Be kept up to date', 'is_correct' => 1],
                    [ 'value' => 'Never installed, as they pose a new threat '],
                    [ 'value' => 'Installed when convenient'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'Besides the operating system, what else needs to be updated?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Installed apps', 'is_correct' => 1 ],
                    [ 'value' => 'Phone case color'],
                    [ 'value' => 'Contact list'],
                    [ 'value' => 'Synced multimedia'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'Which of the following is a potential concern of a data breach?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Loss of public information' ],
                    [ 'value' => 'Both of these'],
                    [ 'value' => 'Neither of these'],
                    [ 'value' => 'Loss of customer confidence', 'is_correct' => 1],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'Which of the following would be the best password for a mobile hotspot?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => '[7TVFX4w%X33G', 'is_correct' => 1 ],
                    [ 'value' => 'password'],
                    [ 'value' => 'joineme'],
                    [ 'value' => 'rebblue'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'True or False: An attacker may still be able to get useful information from less important, unencrypted files.',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'True','is_correct' => 1 ],
                    [ 'value' => 'False'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'If an attacker were able to intercept someone’s communications over Wi-Fi, what sort of damaging information could they learn?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'All of these', 'is_correct' => 1 ],
                    [ 'value' => 'Customer personal information'],
                    [ 'value' => 'Bank account numbers'],
                    [ 'value' => 'Internal work email addresses'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'One of the dangers of public Wi-Fi is the possibility of sending sensitive information over ______ channels.',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Insecure (No Wifi Password)', 'is_correct' => 1 ],
                    [ 'value' => 'Innovative'],
                    [ 'value' => 'Unlicensed'],
                    [ 'value' => 'Secure'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'A form of attack common with insecure Wi-Fi networks is a man-in-the-middle attack. In this, the attacker secretly intercepts and relays a victim’s message to its intended recipient. What sort of damage might this cause?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'All of these', 'is_correct' => 1 ],
                    [ 'value' => 'Financial damage'],
                    [ 'value' => 'Reputational damage'],
                    [ 'value' => 'Data breach'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'True or False: Encryption is the process of scrambling a message or information in such a way that only authorized parties can access it and those who are not authorized cannot.',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'True', 'is_correct' => 1 ],
                    [ 'value' => 'False'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'An app that performs unexpected behavior and asks for unusual permissions, possibly causing data theft or unauthorized charges is:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'A rogue app and should be immediately deleted', 'is_correct' => 1 ],
                    [ 'value' => 'An experimental app'],
                    [ 'value' => 'A type of gaming app'],
                    [ 'value' => 'A legitimate app'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'Which of the following has a legitimate reason to ask for access to your phone records?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Your phone-calling app', 'is_correct' => 1 ],
                    [ 'value' => 'An online poker app'],
                    [ 'value' => 'A music app'],
                    [ 'value' => 'A weather app'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'Tom left his phone on his desk to charge while he visited the restroom. A passerby opened his phone and copied a sensitive document from Tom\'s phone. What could have been easily used to prevent this?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Screen lock', 'is_correct' => 1 ],
                    [ 'value' => 'Antivirus'],
                    [ 'value' => 'A protective phone case'],
                    [ 'value' => 'Deactivating Bluetooth'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'To make sure old vulnerabilities cannot be exploited, you should:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Regularly install updates', 'is_correct' => 1 ],
                    [ 'value' => 'Buy a new phone twice a year'],
                    [ 'value' => 'Only use your phone for voice calls'],
                    [ 'value' => 'Your mobile data plan should be updated'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'Which of the following is a potential concern of a data breach?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'All of these', 'is_correct' => 1 ],
                    [ 'value' => 'Loss of important customer data'],
                    [ 'value' => 'Loss of company reputation'],
                    [ 'value' => 'Legal action'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'Rooting is to Android as jailbreaking is to what?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'iPhone', 'is_correct' => 1 ],
                    [ 'value' => 'Android'],
                    [ 'value' => 'Linux'],
                    [ 'value' => 'None of these'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'True or False: Jailbreaking voids the warranty of iOS devices.',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'False'],
                    [ 'value' => 'True', 'is_correct' => 1],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'Which of the following is a sign an app may contain malware?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'It\'s a game' ],
                    [ 'value' => 'No information available about the creator', 'is_correct' => 1],
                    [ 'value' => 'It\'s free'],
                    [ 'value' => 'It\'s pay-to-play'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'iOS apps are made for the comparatively small range of iOS devices, and apps must undergo a vetting process before being available on the app store. Knowing this, what can you infer?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'iOS apps must meet a certain standard', 'is_correct' => 1 ],
                    [ 'value' => 'iOS apps are more likely to be dangerous'],
                    [ 'value' => 'Not all apps must meet a certain standard'],
                    [ 'value' => 'iOS apps are more likely to be broken'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'You are downloading a sketching app on your tablet. Which of the following permissions should raise your suspicion?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Access Touch Pen' ],
                    [ 'value' => 'Access to Contacts', 'is_correct' => 1],
                    [ 'value' => 'Install shortcut'],
                    [ 'value' => 'Uninstall shortcut'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'Saira likes fast Wi-Fi whenever possible, so she has his phone set to work as a hotspot. His password is orange, a common word. Is this safe?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'No. Neither the password nor the hotspot usage is safe', 'is_correct' => 1 ],
                    [ 'value' => 'No. The password she chose is weak'],
                    [ 'value' => 'No. Using his phone as a hotspot is unsafe'],
                    [ 'value' => 'Yes, its safe'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'Attackers may set up unsecured open Wi-Fi networks whose names resemble those of businesses in the area. This is known as what?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Spoofing', 'is_correct' => 1 ],
                    [ 'value' => 'Impersonation'],
                    [ 'value' => 'Phishing'],
                    [ 'value' => 'Malware'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'When an Android phone is rooted, the root user can do what?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'All of these', 'is_correct' => 1 ],
                    [ 'value' => 'Rewrite the OS'],
                    [ 'value' => 'Install any app'],
                    [ 'value' => 'Change user permissions'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'What is one potential concern when downloading apps not on the app store?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Malware', 'is_correct' => 1 ],
                    [ 'value' => 'Social engineering'],
                    [ 'value' => 'Unfriendly user experience'],
                    [ 'value' => 'All of these'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'Ever since Hania downloaded the latest update for her favorite mobile game, the game has been different. It keeps showing her ads that weren’t in that game before. What should Hania do first?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Run an antivirus', 'is_correct' => 1 ],
                    [ 'value' => 'Immediately delete the app'],
                    [ 'value' => 'Contact the app manufacturers'],
                    [ 'value' => 'Nothing to worry ,keep playing'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'Which of the following would NOT be considered sensitive information that an outsider should not have access to?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Company\'s help line number', 'is_correct' => 1 ],
                    [ 'value' => 'Etisalat internal email addresses'],
                    [ 'value' => 'Customer personal records'],
                    [ 'value' => 'Employee health records'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'As soon as an Etisalat device has been confirmed lost, an Etisalat network administrators should do what?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Remove that device\'s access', 'is_correct' => 1 ],
                    [ 'value' => 'Remove that device\'s case'],
                    [ 'value' => 'Report the matter to the police'],
                    [ 'value' => 'All of these'],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'True or false: Encryption is another word for password protection',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'True'],
                    [ 'value' => 'False', 'is_correct' => 1],
                ],
                'category_id' => 4
            ],
            [
                'value' => 'A long list of remembered networks can result in hackers pretending to be one of these networks but on the other hand, it saves time while on the go. Having many remembered networks is therefore',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'IS dangerous and should be avoided', 'is_correct' => 1 ],
                    [ 'value' => 'Is an acceptable risk '],
                    [ 'value' => 'Is dangerous, but very low risk'],
                    [ 'value' => 'Is dangerous if you have no antivirus'],
                ],
                'category_id' => 4
            ],

            //
            // /////////////////////////////////// PASSWORD SECURITY
            //
            [
                'value' => 'A new employee just started and needs access to a computer. You should:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Provide your credentials' ],
                    [ 'value' => 'Help them contact the IT dept. -888', 'is_correct' => 1],
                    [ 'value' => 'Share your password on a sticky note'],
                    [ 'value' => 'Share your password using text message'],
                ],
                'category_id' => 5
            ],
            [
                'value' => 'A strong password is:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'A word found in a dictionary' ],
                    [ 'value' => 'Is hard for people to guess.', 'is_correct' => 1],
                    [ 'value' => 'Is short so it is easy to remember'],
                    [ 'value' => 'Default password set by the manufacturer'],
                ],
                'category_id' => 5
            ],
            [
                'value' => 'Select the strongest password.',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'rupert' ],
                    [ 'value' => '&red$$marmoset!!!nine', 'is_correct' => 1],
                    [ 'value' => 'cHcRX'],
                    [ 'value' => 'blue42set'],
                ],
                'category_id' => 5
            ],
            [
                'value' => 'What is NOT true about a strong password?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Combines your last name and birthday', 'is_correct' => 1 ],
                    [ 'value' => 'Contains uncommon words'],
                    [ 'value' => 'Contains special characters'],
                    [ 'value' => 'Is long and hard to guess'],
                ],
                'category_id' => 5
            ],
            [
                'value' => 'Etisalat requires you to change passwords on a regular basis. You should:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Ignore the rule.' ],
                    [ 'value' => 'Use your desktop dictionary.'],
                    [ 'value' => 'Create strong passwords every time.', 'is_correct' => 1],
                    [ 'value' => 'Add a date to the end of your password.'],
                ],
                'category_id' => 5
            ],
            [
                'value' => 'What is the biggest password security risk?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Grouping passwords based on account types' ],
                    [ 'value' => 'Using unique passwords for every account'],
                    [ 'value' => 'Making a new account every time'],
                    [ 'value' => 'Using the same password for every account', 'is_correct' => 1],
                ],
                'category_id' => 5
            ],
            [
                'value' => 'You believe that someone knows your password. You have already changed your password. If possible, you should:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Not tell anyone.' ],
                    [ 'value' => 'Provide new password to IT dept.'],
                    [ 'value' => 'Inform Security team', 'is_correct' => 1],
                    [ 'value' => 'Change your password again.'],
                ],
                'category_id' => 5
            ],
            [
                'value' => 'You have forgotten your password. You should:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Borrow your coworker\'s password.' ],
                    [ 'value' => 'Go home and take sick leave.'],
                    [ 'value' => 'Reset your password or call IT Helpdesk', 'is_correct' => 1],
                    [ 'value' => 'Use your coworker password.'],
                ],
                'category_id' => 5
            ],
            [
                'value' => 'Select the weakest password.',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'blueapple', 'is_correct' => 1 ],
                    [ 'value' => 'Blu3_@ppl3'],
                    [ 'value' => 'turqu0isefuj1'],
                    [ 'value' => '85%r!5Qnco6O'],
                ],
                'category_id' => 5
            ],
            [
                'value' => 'A password is important because:',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Should be stored in a password book.' ],
                    [ 'value' => 'Should be memorable.'],
                    [ 'value' => 'It proves your identity to computer.', 'is_correct' => 1],
                    [ 'value' => 'Must be all capital letters.'],
                ],
                'category_id' => 5
            ],
            [
                'value' => 'If you want to store your password electronically, you should use:',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'PDF' ],
                    [ 'value' => 'Never store passwords'],
                    [ 'value' => 'Password Wallet', 'is_correct' => 1],
                    [ 'value' => 'Spreadsheet'],
                ],
                'category_id' => 5
            ],
            //
            // /////////////////////////////////// PHISHING
            //
            [
                'value' => 'You receive an email explaining that you won a tablet from an unknown source you have never heard of. You should:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Provide the required information.' ],
                    [ 'value' => 'Follow the links in the email.'],
                    [ 'value' => 'Report and delete the email.', 'is_correct' => 1],
                    [ 'value' => 'Forward the email to your friend too.'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Pick the most suspicious host URL.',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'https://news.google.com/news/' ],
                    [ 'value' => 'https://www.whitehouse.gov'],
                    [ 'value' => 'http://www.cnn.com/'],
                    [ 'value' => 'https://www.l3370wn3du.net', 'is_correct' => 1],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Pick the least suspicious URL.',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'https://news.gogle.com/news/' ],
                    [ 'value' => 'http://www.cnnn.com/'],
                    [ 'value' => 'http://www.cnn.com/2017/12/01/politics/michael-flynn-charged', 'is_correct' => 1],
                    [ 'value' => 'https://www.l3370wn3du.net'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Most browsers allow you to inspect links by:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Pasting the link in a browser.' ],
                    [ 'value' => 'Clicking the link.'],
                    [ 'value' => 'Sending the link to your manager.'],
                    [ 'value' => 'Hovering over the link with mouse.', 'is_correct' => 1],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Which of the following is most likely to be a phishing link?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'http://re8danjk.117.14/karolinska.AAA.exe', 'is_correct' => 1 ],
                    [ 'value' => 'https://www.whitehouse.gov'],
                    [ 'value' => 'https://www.google.com'],
                    [ 'value' => 'https://www.wikipedia.org'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Pick the most suspicious host URL.',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'go0gle.com', 'is_correct' => 1 ],
                    [ 'value' => 'reddit.com/'],
                    [ 'value' => 'twitter.com/'],
                    [ 'value' => 'dropbox.com'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Phishing emails try to:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'All of these', 'is_correct' => 1 ],
                    [ 'value' => 'Gain private Information'],
                    [ 'value' => 'Ask for clicking on links'],
                    [ 'value' => 'Creates curiously'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Phishing emails might try to get you to:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Download files' ],
                    [ 'value' => 'Click links'],
                    [ 'value' => 'Fill out forms'],
                    [ 'value' => 'All of these', 'is_correct' => 1],
                ],
                'category_id' => 6
            ],
            [
                'value' => '_________ are a great way for an attacker to slip malware past someone’s defenses.',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Attachments', 'is_correct' => 1 ],
                    [ 'value' => 'Enhancements'],
                    [ 'value' => 'Phone calls'],
                    [ 'value' => 'All of these'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Which of the following is NOT a sign that a phone call may be a vishing attempt?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'The caller is polite', 'is_correct' => 1 ],
                    [ 'value' => 'Asks for private email address'],
                    [ 'value' => 'He asks for sensitive information'],
                    [ 'value' => 'The caller is in a hurry'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Usman receives an email offering free movie tickets if he downloads an attached file and forwards it to three friends. Is this phishing?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'No' ],
                    [ 'value' => 'Yes', 'is_correct' => 1],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'True or False: The difference between phishing and social engineering is that social engineering is conducted face-to-face, while phishing happens over the Internet.',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'False', 'is_correct' => 1],
                    [ 'value' => 'True'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Kyle discovers that an Etisalat’s official numbers has been stolen out of the garbage, and now the company directors are receiving phishing calls. What trick did the attacker use to learn valuable information?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Dumpster-diving', 'is_correct' => 1 ],
                    [ 'value' => 'Shoulder-surfing'],
                    [ 'value' => 'Social engineering'],
                    [ 'value' => 'Malware'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'An email from an unfamiliar address offers you a free laptop. What do you do?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Delete it and forget about it' ],
                    [ 'value' => 'Forward it to your coworkers for a laugh'],
                    [ 'value' => 'Hey! Free laptop!'],
                    [ 'value' => 'Report and delete the phishing attempt', 'is_correct' => 1],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Sukaina receives an email from the UAE government stating that she owes additional payments on her taxes. If she fails to pay, she may be audited; however, she can submit her first payment by clicking a link in the email. What is one red flag in this email?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'The government does not email people', 'is_correct' => 1],
                    [ 'value' => 'Asking to pay a government debt online'],
                    [ 'value' => 'Neither of these'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Is there a difference between phishing and spam email?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Yes', 'is_correct' => 1],
                    [ 'value' => 'No'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'When a spear phishing message is aimed at a big, important target such as a CEO or a celebrity, is called as?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Phishing' ],
                    [ 'value' => 'Whaling', 'is_correct' => 1],
                    [ 'value' => 'Clone phishing'],
                    [ 'value' => 'Jellyphishing'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'If a malicious email attachment is downloaded, you could risk what?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Malware', 'is_correct' => 1 ],
                    [ 'value' => 'Antisocial engineering'],
                    [ 'value' => 'Wasping'],
                    [ 'value' => 'Low Battery'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'True or False: A type of phishing aimed at specific people or groups of people is generally known as whaling.',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'False', 'is_correct' => 1],
                    [ 'value' => 'True'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'True or False: A type of phishing done by phone call is known as SMiShing.',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'True' ],
                    [ 'value' => 'False', 'is_correct' => 1],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Pick the least suspicious URL.',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'abcnews.goo.com/Politics/2012-congress-settle-bank-claim/', 'is_correct' => 1 ],
                    [ 'value' => 'https://www.Arnazon.ae'],
                    [ 'value' => 'rt.com/po1itics/'],
                    [ 'value' => 'cnnn.com.org'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'A hacker has stolen Ali’s website by changing the registration without his consent. The hacker now has control of it. What is one attack that he could launch with a hijacked site?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'All of these', 'is_correct' => 1 ],
                    [ 'value' => 'Inserting malware into the ads on site'],
                    [ 'value' => 'Collecting users\' information'],
                    [ 'value' => 'Collecting site visitors\' information'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'True or False: Attackers can spoof email addresses to look like different ones.',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'True', 'is_correct' => 1],
                    [ 'value' => 'False'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'To confirm the legitimacy of an email, you should:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Reply to ask for additional proof' ],
                    [ 'value' => 'Download attachments and scan them '],
                    [ 'value' => 'Contact the alleged sender to confirm', 'is_correct' => 1],
                    [ 'value' => 'All of these'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Spear phishing is the practice of sending fraudulent emails allegedly from a known or trusted sender in order to induce targeted individuals to reveal confidential information.
Spear phishing often involves the hacker:
',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Visiting your house to steal personal information.' ],
                    [ 'value' => 'Getting personal information from social media ', 'is_correct' => 1],
                    [ 'value' => 'Intercepting your instant messaging conversations.'],
                    [ 'value' => 'Stealing packages from your front porch.'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'One way to spot a phisher is to watch for _________ details.',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Inconsistent', 'is_correct' => 1 ],
                    [ 'value' => 'Insistent'],
                    [ 'value' => 'Notable'],
                    [ 'value' => 'Unnotable'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'You receive an email that contains information about your family, projects or other personal details. This email:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Is more likely to be legitimate' ],
                    [ 'value' => 'Is less likely to be legitimate'],
                    [ 'value' => 'Is most likely from a friend'],
                    [ 'value' => 'Could be a spear phishing attempt.', 'is_correct' => 1],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Rashid receives an email from an old friend who is having a hard time and needs a loan immediately. It looks legitimate, but it could also be a phishing attempt. What should he do?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Send him the money' ],
                    [ 'value' => 'Contact his friend through phone call', 'is_correct' => 1],
                    [ 'value' => 'Ignore the message'],
                    [ 'value' => 'Reply to the email and ask for more details'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'True or False: Under some circumstances, phishing may be conducted by someone currently within the organization.',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'False' ],
                    [ 'value' => 'True', 'is_correct' => 1],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'When hovering the mouse over a suspicious link, you will see the full link displayed either in a box or in ____________.',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'The address bar' ],
                    [ 'value' => 'The bottom of the screen', 'is_correct' => 1],
                    [ 'value' => 'The secondary address bar'],
                    [ 'value' => 'A word file'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'True or False: A type of phishing aimed at specific people or groups of people is generally known as whaling.',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'False', 'is_correct' => 1],
                    [ 'value' => 'True'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'True or False: A type of phishing done through text messaging is known as wasping.',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'False', 'is_correct' => 1],
                    [ 'value' => 'True'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Social engineers and phishers often use OSINT, or "open-source intelligence." This is information about the target which has been gathered from public sources. Which of the following is NOT an example of OSINT?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Healthcare records', 'is_correct' => 1 ],
                    [ 'value' => 'Social media'],
                    [ 'value' => 'Company website'],
                    [ 'value' => 'Job postings'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'Hannah receives an email from her gyms general email address. It just says, “Hello, how are you. I need help. Please email me back.” What should she do?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Do not click & call the gym to warn them about possible phishing', 'is_correct' => 1 ],
                    [ 'value' => 'Respond to it, asking for more details'],
                    [ 'value' => 'Block the email address'],
                    [ 'value' => 'Ignore it'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'What is one reason why a toolbar might be dangerous?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Funnel browsing information back to its creator', 'is_correct' => 1 ],
                    [ 'value' => 'It might block malware'],
                    [ 'value' => 'It might change the appearance of your browser'],
                    [ 'value' => 'It is not dangerous'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'To inspect a link you need to look closely at:',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'The country the link is from.' ],
                    [ 'value' => 'Whether there are three w\'s before the first period.'],
                    [ 'value' => 'Host Domain Name ', 'is_correct' => 1],
                    [ 'value' => 'Everything after the "/"'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'True or False: A form of phishing, known as pharming, involves redirecting the victim to a phony version of a known site in order to harvest the victim’s credentials.',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'True', 'is_correct' => 1],
                    [ 'value' => 'False'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'If vishing or “voice phishing” is phishing conducted over phone calls, then how is SMiShing conducted?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Over SMS text message', 'is_correct' => 1 ],
                    [ 'value' => 'Courier or Parcels'],
                    [ 'value' => 'Through smiles (emoji’s)'],
                    [ 'value' => 'This is not a real type of phishing'],
                ],
                'category_id' => 6
            ],
            [
                'value' => 'A type of phishing involving sending copies of previously legitimate messages is known as what?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Clone phishing', 'is_correct' => 1 ],
                    [ 'value' => 'Spearphishing'],
                    [ 'value' => 'Whaling'],
                    [ 'value' => 'Dynamite phishing'],
                ],
                'category_id' => 6
            ],

            //
            // /////////////////////////////////// Physical Security
            //
            [
                'value' => 'Bad physical security practices has on impact on cybersecurity.',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'True'],
                    [ 'value' => 'False', 'is_correct' => 1],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'Physical security protects data security',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'True', 'is_correct' => 1],
                    [ 'value' => 'False'],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'What can prevent a hacker from using your PC when away?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Advanced keyboard' ],
                    [ 'value' => 'Closed Circuit Cameras'],
                    [ 'value' => 'CTRL+ALT+DEL', 'is_correct' => 1],
                    [ 'value' => 'Protective laptop case'],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'Sukaina has some important papers she no longer needs. What should be done before disposing them?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Remove the confidential part' ],
                    [ 'value' => 'None of these'],
                    [ 'value' => 'Use them for scrap paper'],
                    [ 'value' => 'Shred them', 'is_correct' => 1],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'Which of the following is an unsafe place to hide a written password?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Under the keyboard', 'is_correct' => 1 ],
                    [ 'value' => 'In a locked drawer'],
                    [ 'value' => 'In a safe'],
                    [ 'value' => 'A password wallet'],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'Dumpster diving means looking through someone’s physical trash for valuable information. The best way to protect your company from dumpster-divers is to do what?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Supply all dumpsters with locks /alarms' ],
                    [ 'value' => 'Always work from home'],
                    [ 'value' => 'Securely destroy information before discarding', 'is_correct' => 1],
                    [ 'value' => 'Station security guards at garbage containers'],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'You are using your Etisalat card to access a secure Etisalat building. A coworker you know asks you to hold the door. How should you proceed?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Let them in' ],
                    [ 'value' => 'Report them to security'],
                    [ 'value' => 'Ask them to scan their card first', 'is_correct' => 1],
                    [ 'value' => 'Ask your boss'],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'Dumpster diving means looking through someone’s physical trash for valuable information. What is a possible defense against dumpster diving?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Clear desk policy' ],
                    [ 'value' => 'Shredding documents', 'is_correct' => 1],
                    [ 'value' => 'Regular backups'],
                    [ 'value' => 'Antivirus'],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'You have a flash drive with sensitive data on it. You want to recycle the flash drive. How should you proceed?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Place it in the recycling bin' ],
                    [ 'value' => 'Store flash drives under lock'],
                    [ 'value' => 'Consult IT to permanently remove the sensitive data', 'is_correct' => 1],
                    [ 'value' => 'Delete the files on the flash drive'],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'Steve thinks his official smartphone was stolen from his pocket, but he could have misplaced it at his home. However, it did contain encrypted customer information. Should Steve report this?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Yes', 'is_correct' => 1],
                    [ 'value' => 'No'],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'Multi-factor authentication uses two or more factors related to something you have, something you know or something you are. What is one potential challenge of the Knowledge authentication factor?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Knowledge can be forgotten', 'is_correct' => 1 ],
                    [ 'value' => 'Knowledge is inherently insecure'],
                    [ 'value' => 'Knowledge always changes'],
                    [ 'value' => 'Knowledge is not considered secure'],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'Which of the following is a good security practice regarding printers?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Replace printers once a year' ],
                    [ 'value' => 'Quickly pick up printed documents', 'is_correct' => 1],
                    [ 'value' => 'Always print at least two copies'],
                    [ 'value' => 'Keep printers offline when not in use'],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'Sensitive information stored on mobile devices can be protected with what?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Encryption', 'is_correct' => 1 ],
                    [ 'value' => 'Screen protector'],
                    [ 'value' => 'Water resistant cover'],
                    [ 'value' => 'All of the above'],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'Important considerations for data storage include:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Encryption' ],
                    [ 'value' => 'Physical security'],
                    [ 'value' => 'Access permissions'],
                    [ 'value' => 'All of these', 'is_correct' => 1],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'Piggybacking occurs when an authorized person allows someone to follow them through a door to secure area. Is this allowed?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Yes. He has agreed and it is allowed.'],
                    [ 'value' => 'No. The person might have been tricked to get the access.', 'is_correct' => 1],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'Piggybacking is the act of getting into a secured area with the help of someone who’s been fooled or tricked, while tailgating is the act of getting into a secured area by following someone who is unaware. Therefore, piggybacking is __________ and tailgating is __________.',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Voluntary, involuntary' , 'is_correct' => 1],
                    [ 'value' => 'Involuntary, voluntary'],
                    [ 'value' => 'Legal, illegal'],
                    [ 'value' => 'Illegal, legal'],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'You enter a password to access your laptop. What would be a likely attack that a hacker would use to gain access to your laptop?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Sequel injection' ],
                    [ 'value' => 'Phishing'],
                    [ 'value' => 'Dumpster Diving'],
                    [ 'value' => 'Shoulder surfing', 'is_correct' => 1],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'Multi-factor authentication uses two or more factors for authentication, something you have (Access Card), something you know (password) or something you are (fingerprint). What is one potential weakness of these authentication factors?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'They do not protect from tailgating', 'is_correct' => 1 ],
                    [ 'value' => 'They can be easily counterfeited'],
                    [ 'value' => 'They are not considered secure'],
                    [ 'value' => 'They are discriminatory'],
                ],
                'category_id' => 7
            ],
            [
                'value' => 'A member of the marketing team has a habit turning the phone speaker on while talking to others, Is this a security risk? Why?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Yes. Passersby could overhear confidential information', 'is_correct' => 1 ],
                    [ 'value' => 'No. Everybody in the office is already part of the company'],
                    [ 'value' => 'No, but it is a digital security risk'],
                    [ 'value' => 'None of these'],
                ],
                'category_id' => 7
            ],

            //
            // /////////////////////////////////// Social Engineering
            //
            [
                'value' => 'Why would an attacker try to make you act immediately?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Because they are in a hurry'],
                    [ 'value' => 'Make you act at a specific time'],
                    [ 'value' => 'An attacker is not likely to do this'],
                    [ 'value' => 'Limits your time to think and verify', 'is_correct' => 1],
                ],
                'category_id' => 8
            ],
            [
                'value' => 'Social engineering can occur through:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Online Chat'],
                    [ 'value' => 'Phone call'],
                    [ 'value' => 'Text message'],
                    [ 'value' => 'All of these', 'is_correct' => 1],
                ],
                'category_id' => 8
            ],
            [
                'value' => 'A type of request that could indicate social engineering is a request for:',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'A confidential document', 'is_correct' => 1],
                    [ 'value' => 'A price quote'],
                    [ 'value' => 'A product sample'],
                    [ 'value' => 'Etisalat’s public phone number'],
                ],
                'category_id' => 8
            ],
            [
                'value' => 'Which of the following is an indicator of a possible social engineering attack?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Request from a stranger to perform a suspicious activity', 'is_correct' => 1],
                    [ 'value' => 'A well-known customer changes their contact information'],
                    [ 'value' => 'A person is dressed too nicely for the occasion'],
                    [ 'value' => 'A person is wearing a shirt with the company logo'],
                ],
                'category_id' => 8
            ],
            [
                'value' => 'Which of the following is a defense for social engineering?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Never take any action without asking a manager'],
                    [ 'value' => 'Challenge the requestor\'s identity', 'is_correct' => 1],
                    [ 'value' => 'Use landline phones only'],
                    [ 'value' => 'Avoid technological solutions'],
                ],
                'category_id' => 8
            ],
            [
                'value' => 'Which of the following is an example of emotional blackmail?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => '"I don\'t understand why you won\'t help me"', 'is_correct' => 1],
                    [ 'value' => '"I\'ll need that by Friday"'],
                    [ 'value' => '"Did you see the game last night?"'],
                    [ 'value' => 'All of these'],
                ],
                'category_id' => 8
            ],
            [
                'value' => 'Social engineering is:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'A common security threat', 'is_correct' => 1],
                    [ 'value' => 'A low priority threat'],
                    [ 'value' => 'The only threat to security'],
                    [ 'value' => 'Not an issue in the digital age'],
                ],
                'category_id' => 8
            ],
            [
                'value' => 'Why should you always be wary of downloads, updates or add-ons that promise too much?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'They may be trying to entice you into paying money for an inferior product'],
                    [ 'value' => 'They may be trying to use reverse psychology on you'],
                    [ 'value' => 'They may be trying to entice you into doing something dangerous', 'is_correct' => 1],
                    [ 'value' => 'None of these'],
                ],
                'category_id' => 8
            ],
            [
                'value' => 'Social engineering is:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Is not relevant in the digital age'],
                    [ 'value' => 'A trick to get someone to perform dangerous actions', 'is_correct' => 1],
                    [ 'value' => 'Computer hacking'],
                    [ 'value' => 'Is a whale phishing attack'],
                ],
                'category_id' => 8
            ],
            [
                'value' => 'A type of request that could indicate social engineering is a request to:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Schedule a product demo'],
                    [ 'value' => 'Grant Access to a system', 'is_correct' => 1],
                    [ 'value' => 'Speak to a manager'],
                    [ 'value' => 'Open a support ticket'],
                ],
                'category_id' => 8
            ],
            [
                'value' => 'Which of the following is an indicator of a possible social engineering attack?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Asking for an appointment'],
                    [ 'value' => 'A request sent via email'],
                    [ 'value' => 'A request to have a phone conversation'],
                    [ 'value' => 'A request that demands urgency and immediacy', 'is_correct' => 1],
                ],
                'category_id' => 8
            ],
            [
                'value' => 'A man is very offended that you won’t let him into your office without identification, and accuses you of being deliberately offensive. He states that he intends to file a complaint against you. You are worried that this could cause trouble with Human Resources. Should you let him in?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Yes. You do not want any trouble'],
                    [ 'value' => 'No. He is attempting to force you, which makes him suspicious', 'is_correct' => 1],
                ],
                'category_id' => 8
            ],
            [
                'value' => 'You receive a phone call asking you for access to system that your team owns. When you ask information that could help confirm the requestor\'s identity the requester becomes upset. Based on this information what statement is most true?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'This is not suspicious'],
                    [ 'value' => 'The requester should be upset because you are delaying his request'],
                    [ 'value' => 'This person should have their account closed'],
                    [ 'value' => 'This interaction is likely a social engineering attack', 'is_correct' => 1],
                ],
                'category_id' => 8
            ],
            [
                'value' => 'You receive a text message from a number you do not recognize. It asks you to upload confidential information to the Internet on a link. It also states that the message is from your manager, and that she is borrowing a phone because she lost hers. Which would be the safest response?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Before proceeding, confirm the identity of the requester', 'is_correct' => 1],
                    [ 'value' => 'Upload the information immediately'],
                    [ 'value' => 'Delete the text'],
                    [ 'value' => 'Give the requester username/password for access'],
                ],
                'category_id' => 8
            ],
            [
                'value' => 'You receive an email from what appears to be from your bank. You think the email may be suspicious. Which of the following is a safe way to proceed?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Send a letter to the address listed in the email'],
                    [ 'value' => 'Contact the sender through a method not described in the email', 'is_correct' => 1],
                    [ 'value' => 'Reply to the email'],
                    [ 'value' => 'Call the phone number listed in the email'],
                ],
                'category_id' => 8
            ],


            //
            // /////////////////////////////////// Social Media
            //
            [
                'value' => 'True or False: Social media provides a venue for social engineering.',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'False'],
                    [ 'value' => 'True', 'is_correct' => 1],
                ],
                'category_id' => 9
            ],
            [
                'value' => 'True or False: People are more likely to rapidly click links on social media than while browsing.',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'False'],
                    [ 'value' => 'True', 'is_correct' => 1],
                ],
                'category_id' => 9
            ],
            [
                'value' => 'True or False: Social media is entirely safe, provided the user does not click on any suspicious links.',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'True'],
                    [ 'value' => 'False', 'is_correct' => 1],
                ],
                'category_id' => 9
            ],
            [
                'value' => 'What is one danger associated with an employee sharing information about themselves on social media?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Becoming distracted by memes'],
                    [ 'value' => 'Accidentally wasting time'],
                    [ 'value' => 'Becoming a target for social engineering', 'is_correct' => 1],
                    [ 'value' => 'None of these'],
                ],
                'category_id' => 9
            ],
            [
                'value' => 'When using social media, people are not always what?',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Honest'],
                    [ 'value' => 'Who they say they are'],
                    [ 'value' => 'Both of these', 'is_correct' => 1],
                    [ 'value' => 'Neither of these'],
                ],
                'category_id' => 9
            ],
            [
                'value' => 'When using social media, never accept _________ from people you do not know.',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Memes'],
                    [ 'value' => 'Comments'],
                    [ 'value' => 'Friend requests', 'is_correct' => 1],
                    [ 'value' => 'All of these'],
                ],
                'category_id' => 9
            ],
            [
                'value' => 'Never provide ___________ online.',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'Public information'],
                    [ 'value' => 'Personal information', 'is_correct' => 1],
                    [ 'value' => 'Malware'],
                    [ 'value' => 'Socialization'],
                ],
                'category_id' => 9
            ],
            [
                'value' => 'True or False: Social media is one of the most common tools utilized by phishers and social engineers.',
                'difficulty' => 0,
                'answers' => [
                    [ 'value' => 'False'],
                    [ 'value' => 'True', 'is_correct' => 1],
                ],
                'category_id' => 9
            ],
            [
                'value' => 'What sort of information, shared on personal or company social media accounts, might result in a security breach?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'A picture of the team having lunch together outside work'],
                    [ 'value' => 'A favorite music video'],
                    [ 'value' => 'A picture of the team having lunch inside the building', 'is_correct' => 1],
                    [ 'value' => 'The company\'s customer service hotline'],
                ],
                'category_id' => 9
            ],
            [
                'value' => 'True or False: Social engineering is the reason for a very large percentage of successful phishing attacks.',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'False'],
                    [ 'value' => 'True', 'is_correct' => 1],
                ],
                'category_id' => 9
            ],
            [
                'value' => 'What sort of information, shared on personal or company social media accounts, might result in a security breach?',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Personal mobile number', 'is_correct' => 1],
                    [ 'value' => 'Team picture having lunch together outside work'],
                    [ 'value' => 'A favorite music video'],
                    [ 'value' => 'The company\'s customer service hotline'],
                ],
                'category_id' => 9
            ],
            [
                'value' => 'A major threat on social media is that of social _________.',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'Engineering', 'is_correct' => 1],
                    [ 'value' => 'Avoidance'],
                    [ 'value' => 'Deception'],
                    [ 'value' => 'Isolation'],
                ],
                'category_id' => 9
            ],
            [
                'value' => 'Attackers are able to easily get vital, personal information from:',
                'difficulty' => 1,
                'answers' => [
                    [ 'value' => 'The Internet, especially social media', 'is_correct' => 1],
                    [ 'value' => 'News archives'],
                    [ 'value' => 'The library'],
                    [ 'value' => 'Your Manager'],
                ],
                'category_id' => 9
            ],
            [
                'value' => 'One method you can use to prevent social engineering is:',
                'difficulty' => 2,
                'answers' => [
                 [ 'value' => 'Only talk to people in person'],
                    [ 'value' => 'Challenge identity', 'is_correct' => 1],
                    [ 'value' => 'Always be courteous'],
                    [ 'value' => 'Use a fax machine to make all requests'],
                ],
                'category_id' => 9
            ],
            [
                'value' => 'What is one danger associated with a staff sharing information about themselves on social media?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'Becoming a target for impersonation', 'is_correct' => 1],
                    [ 'value' => 'Becoming a target for antisocial behaviors'],
                    [ 'value' => 'Accidentally wasting time'],
                    [ 'value' => 'None of these'],
                ],
                'category_id' => 9
            ],
            [
                'value' => 'Imagine you are a hacker trying to target staff who work remotely. You want to discover more about someone you have seen working in public so that you can exploit his system. Where might you go to learn about him?',
                'difficulty' => 2,
                'answers' => [
                    [ 'value' => 'All of these', 'is_correct' => 1],
                    [ 'value' => 'His social media account pages'],
                    [ 'value' => 'His bio on job websites kept public'],
                    [ 'value' => 'Eavesdrop on his conversations'],
                ],
                'category_id' => 9
            ],
        ];



        foreach ($data as $index => $question){
            if(count($question['answers']) > 2){
                $newquestion = \App\Question::create([
                    'value'=>$question['value'],
                    'category_id'=>$question['category_id'],
                    'difficulty'=>$question['difficulty']
                ]);
                foreach ($question['answers'] as $index => $answer){
                    $newquestion->answers()->create($answer);
                }
            }
        }

    }
}
