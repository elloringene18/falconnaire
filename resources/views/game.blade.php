<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{ asset('public/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/fonts.css') }}">

    <meta name="theme-color" content="#fafafa">
    <style>

        body {
            font-family: 'NeoSansStdRegular';
        }

        section {
            height: 100%;
            width: 100%;
            position: absolute;
            background-color: #c5dff6;
            display: none;
            background-image: url({{ asset('public/img/main-bg.jpg') }});
            background-position: center;
            background-repeat: repeat;
            background-size: auto 100%;
            overflow: hidden;
        }

        section.active {
            display: block;
        }
        .wrap {
            width: 800px;
            position: absolute;
            height: auto;
            top: 60%;
            margin-left: -400px;
            display: none;
            left: 50%;
            z-index: 3;
        }

        .answers {
            margin-top: 60px;
        }

        .answers button {
            width: 90%;
            margin-left: 5%;
            float: left;
            text-align: center;
            margin-bottom: 20px;
            border: 1px solid #cadb2b;
            border-radius: 5px;
            color: #fff;
            background-color: rgba(0,0,0,0.5);
            font-size: 18px;
            padding: 10px;
            box-shadow: 0px 0px 20px #cadb2b;
            transition: all 1s linear;
            position: relative;
        }

        .answers button:focus {
            outline: none;
        }

        .answers button:before {
            content: '';
            background-image: url({{asset('public/img/line-left.png')}});
            width: 300px;
            position: absolute;
            left: -300px;
            top: 50%;
            margin-top: -2px;
            display: block;
            height: 3px;
        }

        .answers button:after {
            content: '';
            background-image: url({{asset('public/img/line-right.png')}});
            width: 300px;
            position: absolute;
            right: -300px;
            top: 50%;
            margin-top: -2px;
            display: block;
            height: 3px;
            background-position-x: right;
        }

        .answers button.active {
            background-color: #cadb2b;
            color: #000;
        }

        .answers button.unshown {
            display: none;
        }

        .answers button.answerCorrect {
            background-color: #218600;
            color: #fff;
        }

        .answers button.answerWrong {
            background-color: #68030d;
            color: #fff;
        }

        .controls {
            position: absolute;
            bottom: 0;
            width: 100%;
            text-align: center;
            padding: 30px;
            z-index: 3;
        }

        .scores {
            position: absolute;
            top: 0;
            width: 100%;
            text-align: center;
            padding: 0 30px 30px;
            z-index: 3;
        }

        .scores .container {
            margin-top: 10%;
        }

        #teama {
            position: absolute;
            padding: 0 20px;
            font-size: 30px;
            left: 20%;
        }

        #teamb {
            position: absolute;
            right: 20%;
            padding: 0 20px;
            font-size: 30px;
        }

        #currentTeam {
            position: absolute;
            left: 0;
            width: 100%;
            font-size: 30px;
            color: #fff;
            font-weight: bold;
            -webkit-text-stroke: 1px black;
            -webkit-text-fill-color: white;
            top: -70px;
        }

        #timer {
            font-size: 40px;
            color: orange;
            position: absolute;
            left: 50%;
            background-image: url({{ asset('public/img/timer.png') }});
            width: 200px;
            height: 200px;
            background-repeat: no-repeat;
            background-size: 100% auto;
            background-position: center;
            margin-left: -100px;
            top: -20px;
            line-height: 82px;
        }

        #timer.danger {
            color: red;
            font-weight: bold;
        }

        #next {
            color: #fff;
            background-color: transparent;
            border: 0;
            text-transform: uppercase;
            font-size: 12px;
            z-index: 4;
        }

        #next.disabled {
            color: #7e7e7e;
        }

        #questions .question {
            display: none;
            transition: all 1s linear;
            border: 2px solid #cadb2b;
            border-radius: 20px;
            color: #fff;
            background-color: rgba(0,0,0,0.5);
            font-size: 20px;
            padding: 30px;
            box-shadow: 0 0 20px #cadb2b;
            font-weight: bold;
        }

        #game.danger .question {
            border: 2px solid red;
            box-shadow: 0 0 20px red;
        }

        #game.danger .answers button {
            border: 2px solid red;
            box-shadow: 0 0 20px red;
        }

        #timer {
            font-size: 50px;
            color: #fff;
            padding: 59px;
        }

        .bounce-in-top{-webkit-animation:bounce-in-top .5s both;animation:bounce-in-top .5s both}

        .bounce-in-top-1 {-webkit-animation:bounce-in-top .5s both;animation:bounce-in-top .5s both}
        .bounce-in-top-2 {-webkit-animation:bounce-in-top .5s both 3s;animation:bounce-in-top .5s both 3s}
        .bounce-in-top-3 {-webkit-animation:bounce-in-top .5s both 6s;animation:bounce-in-top .5s both 6s}
        .bounce-in-top-4 {-webkit-animation:bounce-in-top .5s both 9s;animation:bounce-in-top .5s both 9s}
        .bounce-in-top-5 {-webkit-animation:bounce-in-top .5s both 12s;animation:bounce-in-top .5s both 12s}

        .jello-horizontal{-webkit-animation:jello-horizontal .9s infinite both;animation:jello-horizontal .9s infinite both; display: none;}
        @-webkit-keyframes jello-horizontal{0%{-webkit-transform:scale3d(1,1,1);transform:scale3d(1,1,1)}30%{-webkit-transform:scale3d(1.25,.75,1);transform:scale3d(1.25,.75,1)}40%{-webkit-transform:scale3d(.75,1.25,1);transform:scale3d(.75,1.25,1)}50%{-webkit-transform:scale3d(1.15,.85,1);transform:scale3d(1.15,.85,1)}65%{-webkit-transform:scale3d(.95,1.05,1);transform:scale3d(.95,1.05,1)}75%{-webkit-transform:scale3d(1.05,.95,1);transform:scale3d(1.05,.95,1)}100%{-webkit-transform:scale3d(1,1,1);transform:scale3d(1,1,1)}}@keyframes jello-horizontal{0%{-webkit-transform:scale3d(1,1,1);transform:scale3d(1,1,1)}30%{-webkit-transform:scale3d(1.25,.75,1);transform:scale3d(1.25,.75,1)}40%{-webkit-transform:scale3d(.75,1.25,1);transform:scale3d(.75,1.25,1)}50%{-webkit-transform:scale3d(1.15,.85,1);transform:scale3d(1.15,.85,1)}65%{-webkit-transform:scale3d(.95,1.05,1);transform:scale3d(.95,1.05,1)}75%{-webkit-transform:scale3d(1.05,.95,1);transform:scale3d(1.05,.95,1)}100%{-webkit-transform:scale3d(1,1,1);transform:scale3d(1,1,1)}}

        @-webkit-keyframes bounce-in-top{0%{-webkit-transform:translateY(-500px);transform:translateY(-500px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in;opacity:0}38%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out;opacity:1}55%{-webkit-transform:translateY(-65px);transform:translateY(-65px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}72%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}81%{-webkit-transform:translateY(-28px);transform:translateY(-28px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}90%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}95%{-webkit-transform:translateY(-8px);transform:translateY(-8px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}100%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}}@keyframes bounce-in-top{0%{-webkit-transform:translateY(-500px);transform:translateY(-500px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in;opacity:0}38%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out;opacity:1}55%{-webkit-transform:translateY(-65px);transform:translateY(-65px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}72%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}81%{-webkit-transform:translateY(-28px);transform:translateY(-28px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}90%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}95%{-webkit-transform:translateY(-8px);transform:translateY(-8px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}100%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}}

        .slide-in-fwd-center{-webkit-animation:slide-in-fwd-center .4s cubic-bezier(.25,.46,.45,.94) both;animation:slide-in-fwd-center .4s cubic-bezier(.25,.46,.45,.94) both}
        @-webkit-keyframes slide-in-fwd-center{0%{-webkit-transform:translateZ(-1400px);transform:translateZ(-1400px);opacity:0}100%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1}}@keyframes slide-in-fwd-center{0%{-webkit-transform:translateZ(-1400px);transform:translateZ(-1400px);opacity:0}100%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1}}

        .rotate-in-center{-webkit-animation:rotate-in-center .6s cubic-bezier(.25,.46,.45,.94) both;animation:rotate-in-center .6s cubic-bezier(.25,.46,.45,.94) both}
        @-webkit-keyframes rotate-in-center{0%{-webkit-transform:rotate(-360deg);transform:rotate(-360deg);opacity:0}100%{-webkit-transform:rotate(0);transform:rotate(0);opacity:1}}@keyframes rotate-in-center{0%{-webkit-transform:rotate(-360deg);transform:rotate(-360deg);opacity:0}100%{-webkit-transform:rotate(0);transform:rotate(0);opacity:1}}

        .bounce-top {-webkit-animation:bounce-top 1s infinite both;animation:bounce-top 1s infinite both}
        @-webkit-keyframes bounce-top{0%{-webkit-transform:translateY(-45px);transform:translateY(-45px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in;opacity:1}24%{opacity:1}40%{-webkit-transform:translateY(-24px);transform:translateY(-24px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}65%{-webkit-transform:translateY(-12px);transform:translateY(-12px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}82%{-webkit-transform:translateY(-6px);transform:translateY(-6px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}93%{-webkit-transform:translateY(-4px);transform:translateY(-4px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}25%,55%,75%,87%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}100%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out;opacity:1}}@keyframes bounce-top{0%{-webkit-transform:translateY(-45px);transform:translateY(-45px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in;opacity:1}24%{opacity:1}40%{-webkit-transform:translateY(-24px);transform:translateY(-24px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}65%{-webkit-transform:translateY(-12px);transform:translateY(-12px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}82%{-webkit-transform:translateY(-6px);transform:translateY(-6px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}93%{-webkit-transform:translateY(-4px);transform:translateY(-4px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}25%,55%,75%,87%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}100%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out;opacity:1}}

        .swing-in-top-fwd{-webkit-animation:swing-in-top-fwd .5s cubic-bezier(.175,.885,.32,1.275) both;animation:swing-in-top-fwd .5s cubic-bezier(.175,.885,.32,1.275) both}
        @-webkit-keyframes swing-in-top-fwd{0%{-webkit-transform:rotateX(-100deg);transform:rotateX(-100deg);-webkit-transform-origin:top;transform-origin:top;opacity:0}100%{-webkit-transform:rotateX(0deg);transform:rotateX(0deg);-webkit-transform-origin:top;transform-origin:top;opacity:1}}@keyframes swing-in-top-fwd{0%{-webkit-transform:rotateX(-100deg);transform:rotateX(-100deg);-webkit-transform-origin:top;transform-origin:top;opacity:0}100%{-webkit-transform:rotateX(0deg);transform:rotateX(0deg);-webkit-transform-origin:top;transform-origin:top;opacity:1}}

        .vCenter {
            position: absolute;
            top: 50%;
            left: 50%;
            opacity: 0;
        }

        #welcome h1 {
            color: #fff;
            -webkit-text-stroke: 1px black;
            -webkit-text-fill-color: white;
            font-weight: bold;
            font-size: 36px;
        }

        #welcome h5 {
            color: #fff;
            -webkit-text-stroke: 1px black;
            -webkit-text-fill-color: white;
            font-weight: bold;
            font-size: 24px;
        }

        #welcome select {
            width: 100%;
            background: rgba(0,0,0,0.5);
            padding: 0 15px;
            color: #fff;
            border-radius: 10px;
            border-color: #cadb2b;
            height: 40px;
            line-height: 40px;
        }

        #welcome .flare {
            width: 100%;
            height: 100%;
            position: absolute;
            background-image: url({{ asset('public/img/flare.png') }});
            background-size: auto 100%;
            background-repeat: no-repeat;
            background-position: center;
        }

        #welcome .vCenter {
            width: 100%;
            max-width: 560px;
            margin-top: -285px;
            margin-left: -280px;
        }

        #etisalat-logo {
            position: absolute;
            top: 0;
            left: 10%;
            z-index: 1;
        }

        #show-teams, .start-match, .continue-match, #show-instructions {
            position: absolute;
            display: block;
            width: 100%;
            margin-top: 60px;
            color: #fff;
            text-decoration: none !important;
            text-align: center;
            left: 0;
            bottom: 5%;
            z-index: 50;
        }

        .endcontrols {
            position: absolute;
            display: block;
            width: 100%;
            margin-top: 60px;
            color: #fff;
            text-decoration: none !important;
            text-align: center;
            left: 0;
            bottom: 5%;
        }

        .endcontrols a {
            color: #fff;
            padding: 0 5px;
        }

        #versus .teams {
        }


        #instructions h1 {
            color: #fff;
            margin-top: 30px;
        }

        #instructions .vCenter {
            margin-top: -321px;
            margin-left: -451px;
        }

        #versus .team1 {
            float: left;
            width: 45%;
        }
        #versus .team2 {
            float: right;
            width: 45%;
        }

        #versus .vs  {
            float: right;
            width: 10%;
            position: relative;
        }

        #versus .vs span {
            position: absolute;
            color: #fff;
            font-size: 30px;
            width: 100%;
            text-align: center;
            left: 0;
            display: block;
            margin-left: 0 !important;
        }

        #versus .team1, #versus .team2, #versus .team1 img , #versus .team2  img  {
            position: relative;
        }

        .team1 img , #versus .team2  img  {
            width: 100%;
        }

        #versus .team-name {
            position: absolute;
            width: 260px;
            height: 160px;
            background-color: rgba(0,0,0,0.5);
            border-radius: 20px;
            top: 51px;
            color: #fff;
            text-align: center;
            font-size: 30px;
            text-align: center;
        }

        #versus .team1 .team-name {
            left: -200px;
            padding-right: 50px;
            box-shadow: 1px 1px 20px #c9db2a;
        }

        #versus .team2 .team-name {
            right: -200px;
            padding-left: 50px;
            box-shadow: 1px 1px 20px #c9db2a;
        }

        #versus .team-name span {
            margin-left: 0 !important;
            left: 0;
            width: 100%;
        }

        #versus .team1 span {
            padding-right: 50px;
            padding-left: 20px;
            line-height: 30px;
        }

        #versus .team2 span {
            padding-left: 50px;
            padding-right: 20px;
            line-height: 30px;
        }

        .scores  .cont {
            position: absolute;
            width: 200px;
            height: 110px;
            background-color: rgba(0,0,0,0.5);
            border-radius: 20px;
            top: 21px;
            color: #fff;
            text-align: center;
            font-size: 30px;
            text-align: center;
            box-shadow: 1px 1px 20px #c9db2a;
        }

        #teama img, #teamb img {
            position: relative;
            z-index: 1;
        }

         #teama.active .cont, #teamb.active .cont {
             box-shadow: 1px 1px 10px #cadb2b;
             background-color: #cadb2b;
             color: #000;
        }

        #teama .cont {
            left: -120px;
            padding-right: 50px;
            line-height: 16px;
        }

        #teamb .cont {
            right: -120px;
            padding-left: 50px;
            line-height: 16px;
        }

        .cont .name {
            font-size: 21px;
            line-height: 16px;
            margin: 23px 0 0px;
            display: block;
            font-weight: bold;
        }

        .cont .name.long {
            font-size: 21px;
            line-height: 21px;
            margin: 14px 0 0px;
        }

        .cont .name.verylong {
            font-size: 14px;
            line-height: 21px;
            margin: 14px 0 0px;
        }

        .cont .score {
            margin-top: 20px;
            display: block;
        }

        #game .overlay {
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0;
            opacity: .3;
            z-index: 2;
        }

        #game.danger .overlay {
            background-color: red;
        }

        #game.success .overlay{
            background-color: #01b842;;
        }

        .round img {
            text-align: center;
        }

        #congratulations .cong {
            width: 170%;
            margin-left: -34%;
        }

        #congratulations .wrapper {
            width: 700px;
            margin-left: -350px !important;
            top: 45%;
        }

        #congratulations .top h1 {
            text-align: center;
            margin-top: -66px;
            color: #fff;
            margin-bottom: 0;
        }

        #congratulations .bot h1 {
            text-align: center;
            margin-top: -42px;
            color: #fff;
            font-size: 22px;
        }

        #congratulations .circ {
            position: absolute;
            width: 140%;
        }
        a:hover {
            color: #dae929;
        }
        #congratulations h3 {
            position: absolute;
            width: 100%;
            color: #fff;
            text-align: center;
            left:0;
            top: 30%;
            font-size: 35px;
            font-weight: bold;
        }

        #congratulations .circ.left {
            right: 0;
            margin-right: -20%;
        }

        #congratulations .circ.right {
            left: 0;
            margin-left: -20%;
        }

        #team-register-1 {
            position: absolute;
            left: 0;
            top: 30%;
            width: 20%;
            padding: 20px;
        }

        #team-register-1 input, #team-register-2 input {
            margin-bottom: 10px;
            font-size: 14px;
            width: 100%;
        }

        #team-register-1 h5, #team-register-2 h5 {
            font-size: 18px;
        }

        #team-register-2 {
            position: absolute;
            right: 0;
            top: 30%;
            width: 20%;
            padding: 20px;
        }

        #instructions {
            color: #fff;
        }

        #instructions ul li {
            list-style: none;
            margin-bottom: 10px;
            font-size: 20px;
            background-color: #c8da2a;
            padding: 5px 30px;
            padding-bottom: 10px;
            border-radius: 10px;
            color: #000;
        }

        .box {
            background-image: url({{ asset('public/img/line-bg.png') }});
            background-color: rgba(0,0,0,0.5);
            border-radius: 20px;
            color: #fff;
            font-size: 20px;
            box-shadow: 1px 1px 20px #c9db2a;
            padding: 30px;
            position: absolute;
            z-index: 2;
            text-align: left;
            font-weight: bold;
            width: 60%;
            padding-right: 100px;
            top: 50%;
            left: 50%;
            margin-left: -300px;
        }

        .box h1 {
            font-size: 42px;
            color: #c9db2a !important;
            font-weight: bold;
            margin-bottom: 31px;
        }

        .box ol {
            padding-left: 24px;
        }

        .box ol li {
            margin-bottom: 20px;
            font-size: 20px;
        }

        .box img {
            position: absolute;
            top: 25%;
            right: -31%;
        }

        .green-text {
            color: #c9db2a;
        }

        #bgSound {
            position: absolute;
            z-index: 5;
            color: #fff;
            bottom: 5px;
            left: 5px;
        }

        .score-plus {
            position: absolute;
            right: 40%;
            z-index: 3;
            color: #cadb2b;
            font-weight: bold;
            font-size: 45px;
            top: -50%;
        }

        #teamb .score-plus {
            left: 40%;
        }

        .round h1 {
            position: absolute;
            top: 10%;
            color: #fff;
            width: 100%;
            text-align: center;
            -webkit-text-stroke: 1px black;
            -webkit-text-fill-color: white;
            font-weight: bold
        }

        #level-monitor {
            color: #fff;
            position: absolute;
            width: 100%;
            font-weight: bold;
            top: 155px;
            left: 0;
        }

        #level-monitor .level {
            color: #cadb2b;
        }

        @media only screen and (max-height: 960px) {
            section {
                background-size: 100% auto;
            }

            #welcome img {
                width: 300px;
            }
        }

        @media only screen and (max-height: 840px) {
            .scores .container {
                margin-top: 6%;
            }

            #questions .question {
                padding: 20px;
                font-size: 18px;
            }

            .answers button {
                font-size: 17px;
                padding: 5px;
                margin-bottom: 10px;
            }

            .answers {
                margin-top: 40px;
            }

            .box ol li {
                margin-bottom: 10px;
            }
        }

        @media only screen and (max-height: 740px) {
            #welcome img {
                width: 200px;
            }

            .box ol li {
                margin-bottom: 5px;
            }

            #timer {
                width: 150px;
                height: 150px;
                margin-left: -75px;
                padding: 33px;
                font-size: 40px;
                line-height: 80px;
            }
            #level-monitor {
                top: 115px;
            }
            .wrap {
                width: 80%;
                top: 60%;
                margin-left: -40%;
                left: 50%;
            }
        }


        @media only screen and (max-height: 960px) and (max-width: 1440px) {
            section {
                background-size: auto 100%;
            }
        }

        #selectCats {
            float: none;
            display: inline-block;
            width: 100%;
            background-color: transparent;
            border: 0;
            padding: 0;
            outline: 0 !important;
            color: #c9db2a;
            box-shadow: none;
            margin-top: 30px;
        }
    </style>
</head>

<body>
<!--[if IE]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->
<a href="#" id="bgSound">Enable Audio</a>

<!-- Modal -->
<div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Select Question Categories</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @foreach($categories as $category)
                    <input id="cat{{$category->id}}" type="checkbox" value="{{$category->id}}" class="categories" {{ $category->slug == "general-questions" ? 'disabled="disabled" checked' : '' }}>
                    <label for="cat{{$category->id}}">{{ $category->name }} {{ $category->slug == "general-questions" ? '(Default)' : '' }}</label>
                    <br/>
                @endforeach
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<section id="welcome" class="active">
    <div class="flare rotate-in-center"></div>
    <div class="container text-center">
        <div class="vCenter">
            <h1 class="bounce-in-top">Welcome to</h1>
            <img class="bounce-in-top" src="{{ asset('public/img/logo.png') }}" width="70%">

            <br/>
            <br/>
            <div class="row">
                <div class="col-md-12">
                    <h5>Match:</h5>
                    <select id="schedule">
                        @foreach($schedules as $schedule)
                            <option value="{{ $schedule->id }}">{{ $schedule->date_start->format('M d') }} ({{ $schedule->date_start->format('h:i') }} - {{ $schedule->date_end->format('h:i') }})</option>
                        @endforeach
                    </select>
                    <br/>
                    <br/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h5>Team A:</h5>
                    <select id="in-teama">
                    </select>
                </div>
                <div class="col-md-6">
                    <h5>Team B:</h5>
                    <select id="in-teamb">
                    </select>
                </div>
            </div>
            <div class="row">
                <br/>
                <br/>
                <!-- Button trigger modal -->
                <button type="button" id="selectCats" class="btn btn-primary" data-toggle="modal" data-target="#categoryModal">
                    Select Categories ( <span id="selectedCats">1</span> Selected )
                </button>
            </div>
        </div>


        <a href="#" id="show-instructions">BEGIN</a>
    </div>
</section>

<section id="instructions">
    <div class="container-fluid text-center">
        <div class="box vCenter">
            <h1>How to play</h1>
            <ol>
                <li>Each game has 3 levels <span class="green-text">(easy, medium & hard)</span></li>
                <li>Each level has <span class="green-text">4 questions</span></li>
                <li>When the question is asked, <span class="green-text">the active team has {{ $settings['timer'] }} seconds to answer</span></li>
                <li>If answered correctly, <span class="green-text">the team gets {{ $settings['easy-points'] }}, {{ $settings['medium-points'] }} or {{ $settings['hard-points'] }} points, based on the level</span></li>
                <li>If the answer is wrong, the question <span class="green-text">goes to the other team for half the points and time ({{ $settings['steal-timer'] }} seconds)</span></li>
                <li><span class="green-text">If both teams answer incorrectly</span>, the next question will appear</li>
                <li>At the end, <span class="green-text">the team with the most point wins</span></li>
                <li>If both teams tie, <span class="green-text">the team with the faster time win</span></li>
                <li>All team will be <span class="green-text">placed on a company-wide scoreboard</span></li>
            </ol>
            <img src="{{ asset('public/img/scorebird.png') }}">
        </div>
        <a href="#" id="show-teams">Continue</a>
    </div>
</section>

<section id="versus">
    <div class="container-fluid text-center">
        <div class="vCenter teams">
            <div class="row">
                <div class="col-md-12 text-center">
                    <img class="" src="{{ asset('public/img/logo.png') }}" width="300">
                </div>
            </div>
            <div class="row">
                <div class="team1">
                    <div class="team-name"><span class="vCenter">Team Name 1</span></div>
                    <img src="{{ asset('public/img/team-1.png') }}">
                </div>
                <div class="vs">
                    <span class="vCenter">VS</span>
                </div>
                <div class="team2">
                    <div class="team-name"><span class="vCenter">Team Name 2</span></div>
                    <img src="{{ asset('public/img/team-2.png') }}">
                </div>
            </div>
        </div>
        <a href="#" class="start-match">Start Match</a>
    </div>
</section>

<section class="round easy">
    <h1>Let the games begins!</h1>
    <img src="{{ asset('public/img/easy.png') }}" class="vCenter">
    <a href="#" class="continue-match">Start Match</a>
</section>

<section class="round medium">
    <h1>Moving up! </h1>
    <img src="{{ asset('public/img/medium.png') }}" class="vCenter">
    <a href="#" class="continue-match">Proceed</a>
</section>

<section class="round hard">
    <h1>It’s the final round!</h1>
    <img src="{{ asset('public/img/hard.png') }}" class="vCenter">
    <a href="#" class="continue-match">Proceed</a>
</section>

<section id="game">
    <div class="scores">
        <div class="container text-center position-relative">
            <div id="teama">
                <div class="cont">
                    <span class="name"></span>
                    <span class="score">0</span>
                    <span class="score-plus jello-horizontal"></span>
                </div>
                <img src="{{ asset('public/img/team-1.png') }}" width="150">
            </div>
            <div id="currentTeam">
                <span></span>
            </div>
            <div id="timer"></div>
            <div id="level-monitor"><span class="level">Level: Easy</span><br/>Question <span class="que">1</span> out of 12</div>
            <div id="teamb">
                <img src="{{ asset('public/img/team-2.png') }}" width="150">
                <div class="cont">
                    <span class="name"></span>
                    <span class="score">0</span>
                    <span class="score-plus jello-horizontal"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="container text-center" id="questions">
    </div>

    <div class="controls">
        <button id="next">Next</button>
    </div>

</section>

<section id="congratulations">
    <div class="container">
        <div class="vCenter wrapper">
            <div class="row">
                <div class="top">
                    <img src="{{ asset('public/img/congratulations.png') }}" class="cong">
                    <h1>Congratulations</h1>
                    <br/>
                    <br/>
                    <br/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1">
                </div>
                <div class="col-md-6">
                    <img src="{{ asset('public/img/team-1.png') }}" width="100%">
                </div>
                <div class="col-md-3">
                    <img src="{{ asset('public/img/final-score.png') }}" width="150%" class="circ right">
                    <h3><span id="winner-score"></span>pts</h3>
                </div>
            </div>
            <div class="row">
                <div class="bot">
                    <br/>
                    <br/>
                    <br/>
                    <img src="{{ asset('public/img/winner.png') }}" class="cong">
                    <h1 id="winner-team"></h1>
                    <h5 id="winner-time"></h5>

                </div>
            </div>
        </div>
    </div>
    <div class="endcontrols">
        <a href="{{ url('scoreboard') }}" class="" target="_blank">See Scoreboard</a> |
        <a href="{{ url('game') }}" class="">Start Another Match</a>
    </div>
</section>

<script src="{{ asset('public/js/vendor/modernizr-3.8.0.min.js') }}"></script>
<script src="{{ asset('public/js/vendor/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/js/plugins.js') }}"></script>
<script src="{{ asset('public/js/main.js') }}"></script>

<script>

    var correctSound = new Audio("{{ asset('public/audio/correct_answer.mp3') }}"); // buffers automatically when created
    var wrongSound = new Audio("{{ asset('public/audio/wrong_answer.mp3') }}"); // buffers automatically when created
    var vsSound = new Audio("{{ asset('public/audio/boxing_bell.mp3') }}"); // buffers automatically when created
    var gameIntroBg = new Audio("{{ asset('public/audio/Game_Show.mp3') }}"); // buffers automatically when created
    var questionBg = new Audio("{{ asset('public/audio/White_Hats.mp3') }}"); // buffers automatically when created
    var soundOn = false; // buffers automatically when created

    gameIntroBg.addEventListener('ended', function() {
        this.currentTime = 0;
        this.play();
    }, false);

    questionBg.addEventListener('ended', function() {
        this.currentTime = 0;
        this.play();
    }, false);

    var baseUrl = "{{url('/')}}";

    var teams = [];
    var settings = [];

    settings['ad'] = parseInt('{{ $settings['answer-duration'] }}');
    settings['ep'] = parseInt('{{ $settings['easy-points'] }}');
    settings['mp'] = parseInt('{{ $settings['medium-points'] }}');
    settings['hp'] = parseInt('{{ $settings['hard-points'] }}');


    $('#bgSound').on('click',function(){

        if($(this).hasClass('active')){
            soundOn = false;
            gameIntroBg.pause();
            questionBg.pause();
            $(this).removeClass('active').text('Enable Audio');
        }
        else {
            soundOn = true;
            gameIntroBg.play();
            $(this).addClass('active').text('Disable Audio');
        }

    });

    function updateTeams(){

        $('#in-teama').empty();
        $('#in-teamb').empty();

        $.ajax({
            type: "GET",
            url: baseUrl+'/get-teams-by-schedule/'+$('#schedule').val(),
            success: function(response){

                console.log(response);
                teams[response.teams[0].id] = response.teams[0].name;
                teams[response.teams[1].id] = response.teams[1].name;

                $('#in-teama').append('<option value="'+response.teams[0].id+'">'+response.teams[0].name+'</option>');
                $('#in-teamb').append('<option value="'+response.teams[1].id+'">'+response.teams[1].name+'</option>');
            },
            complete : function (event,error){
                $('#in-teamb option').sort(function(a,b) {
                    return a.value > b.value;
                }).appendTo('#in-teamb');
            }
        });
    }
    updateTeams();

    function getQuestions(){

        catids = [];

        $('.categories:checked').each(function(){
            catids.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: baseUrl+'/get-questions-by-cat',
            data: {
                cats : catids
            },
            success: function(response){
                updateQuestions(response);
            },
            complete : function (event,error){
            }
        });
    }

    function updateQuestions(data){
        points = [];
        points[0] = "{{$settings['easy-points']}}";
        points[1] = "{{$settings['medium-points']}}";
        points[2] = "{{$settings['hard-points']}}";

        Object.keys(data).map(function(objectKeyA, index) {
            var diff = data[objectKeyA];

            Object.keys(diff).map(function(objectKeyB, index) {
                var question = diff[objectKeyB];

                el = '<div class="wrap" data-points="'+ points[question.difficulty] +'">\n' +
                    '    <h1 class="question">'+question.value+'</h1>\n' +
                    '    <div class="answers">\n';


                Object.keys(question.answers).map(function(objectKeyC, index) {
                    var answer = question.answers[objectKeyC];

                    el += '<button class="unshown '+( answer.is_correct ? 'correct' : '')+'">'+answer.value+'</button>\n';
                });

                el +=    '                    \n' +
                    '    </div>\n' +
                    '</div>';

                $('#questions').append(el);
            });
        });
        $('#questions').prepend('<div class="overlay"></div>');
        $('#questions .wrap').first().show();
        addAnswerEvent();
    }

    function vCenter(){
        $('.vCenter').each(function(){
            h = $(this).height();
            w = $(this).width();


            if($(this).hasClass('box')){
                $(this).css('margin-top','-'+[(h/2)+50]+'px');
                $(this).css('margin-left','-'+[(w/2)+150]+'px');
            } else {
                $(this).css('margin-top','-'+h/2+'px');
                $(this).css('margin-left','-'+w/2+'px');
            }

            $(this).css('opacity',1);
        });
    }

    $('#schedule').on('change',function(){
        updateTeams();
    });

    $('#show-teams').on('click',function(){
        $(this).closest('section').removeClass('active');
        $('#versus').addClass('active');

        $('#versus .team1 .team-name span').text(teams[$('#in-teama').val()]);
        $('#versus .team2 .team-name span').text(teams[$('#in-teamb').val()]);

        $('#teama .cont .name').text(teams[$('#in-teama').val()]);
        $('#teamb .cont .name').text(teams[$('#in-teamb').val()]);

        vCenter();
        updateTurn();

        if(soundOn)
            vsSound.play();

        if( teams[$('#in-teama').val()].length>12 && teams[$('#in-teama').val()].length<20)
            $('#teama .name').addClass('long');
        else if( teams[$('#in-teama').val()].length > 10)
            $('#teama .name').addClass('verylong');

        if( teams[$('#in-teamb').val()].length>12 && teams[$('#in-teamb').val()].length<20)
            $('#teamb .name').addClass('long');
        else if( teams[$('#in-teamb').val()].length > 20)
            $('#teamb .name').addClass('verylong');

    });

    $('.start-match').on('click',function(){
        $(this).closest('section').removeClass('active');
        $('.round.easy').addClass('active');
        vCenter();
    });

    $('.continue-match').on('click',function(){
        $(this).closest('section').removeClass('active');
        $('#game').addClass('active');
        gameIntroBg.pause();

        if(soundOn)
            questionBg.play();

        vCenter();
    });

    $('#show-instructions').on('click',function(){

        getQuestions();
        if($('#in-teama').val() != $('#in-teamb').val()){
            $(this).text('');
            $(this).append('<img src="{{asset('public/img/loader.gif')}}" style="width:100px;">');

            $.ajax({
                type: "GET",
                url: baseUrl+'/check-team-matches/'+$('#in-teama').val()+'/'+$('#in-teamb').val(),
                success: function(response){

                    if(response.data){
                        $('#welcome').removeClass('active');
                        $('#instructions').addClass('active');
                        vCenter();
                    } else {
                        alert('One or two of the teams have already played. The game will now refresh to sync data.');
                        location.reload();
                    }

                },
                complete : function (event,error){
                    $('#in-teamb option').sort(function(a,b) {
                        return a.value > b.value;
                    }).appendTo('#in-teamb');
                }
            });

        } else {
            alert('The same team cannot play against it self. Please select opposing teams.');
        }


    });

    /*
    1 = Show Question
    2 = Answers
    3 = Start Timer
    4 = Check Answer
    5 =  Next Question
    */

    currentQuestion = 0;
    currentStep = 1;
    currentTurn = 0;
    currentTeamATime = 0;
    currentTeamAScore = 0;
    currentTeamBTime = 0;
    currentTeamBScore = 0;
    steal = false;

    timerDuration = parseInt('{{ $settings['timer'] }}');
    timerStealDuration = parseInt('{{ $settings['steal-timer'] }}');

    function centerAlign(){
        $('.wrap').each(function(){
            vTop = $(this).height() / 2;
            vTop -= 40;
            $(this).css('margin-top','-'+vTop+'px');
        });
    }
    centerAlign();

    function updateTurn(){
        $('#currentTeam').text($('.scores .name').eq(currentTurn).text()+" Turn");

        if(currentTurn>0){
            $('#teamb').addClass('active');
            $('#teama').removeClass('active');
        } else {
            $('#teama').addClass('active');
            $('#teamb').removeClass('active');
        }
    }
    updateTurn();

    function updateScores(){
        $('#teama .score').text(currentTeamAScore);
        $('#teamb .score').text(currentTeamBScore);

        $.ajax({
            type: "POST",
            url: baseUrl+'/update-team-scores',
            data: {
                team_a_id : $('#in-teama').val(),
                team_b_id : $('#in-teamb').val(),
                team_a_score : currentTeamAScore,
                team_a_time : currentTeamATime,
                team_b_score : currentTeamBScore,
                team_b_time : currentTeamBTime,
                schedule_id : $('#schedule').val(),
            },
            success: function(response){
            }
        });
    }

    function updateQuestion(){
        $('#questions .wrap').eq(currentQuestion).show();
    }
    updateQuestion();

    function updateButton(){
        if(currentStep==1)
            $('#next').text('Show Question').removeClass('disabled');
        if(currentStep==2)
            $('#next').text('Show Answers').removeClass('disabled');
        if(currentStep==21)
            $('#next').text('Showing answers. Please wait...').addClass('disabled');
        if(currentStep==3)
            $('#next').text('Start Timer').removeClass('disabled');
        if(currentStep==4)
            $('#next').text('Check Answer').removeClass('disabled');
        if(currentStep==5)
            $('#next').text('Next Question').removeClass('disabled');

        if(currentStep==5 && currentQuestion==11)
            $('#next').text('See Results').removeClass('disabled');

    }
    updateButton();

    var countdown = 0;
    var timer = null;

    function startTimer(duration){

        $('#timer').removeClass('danger').addClass('bounce-top');

        countdown = duration;
        timer = setInterval(function(){

            if(!steal){
                if(currentTurn)
                    currentTeamBTime++;
                else
                    currentTeamATime++;
            }

            if(!$('#questions .wrap').eq(currentQuestion).find('button.active'))
                $('#next').text('Please select an answer');
            else
                $('#next').text('Check answer');

            $('#timer').text(countdown);

            if(countdown>0){
                countdown--;

                if(countdown<=10)
                    $('#timer').addClass('danger');
                else
                    $('#timer').removeClass('danger');
            }
            else {
                clearInterval(timer);
                timer = null;
                checkAnswer();
            }

        },1000);
    }

    function nextQuestion(){
        currentQuestion++;
        steal = false;
        currentStep = 1;
        countdown = 0;
        timer = null;
        $('#questions .wrap').hide();
        $('#questions .wrap').eq(currentQuestion).show();
        $('#game').removeClass('danger').removeClass('success');
        toggleTurns();

        if(currentQuestion===4){
            $('#game').removeClass('active');
            $('.round.medium').addClass('active');
            $('#level-monitor .level').text("Level: Medium");
            vCenter();
        }
        else if(currentQuestion===8){
            $('#game').removeClass('active');
            $('.round.hard').addClass('active');
            $('#level-monitor .level').text("Level: Hard");
            vCenter();
        }
        else if(currentQuestion===12){
            $('#game').removeClass('active');
            $('#congratulations').addClass('active');

            $('#finalScoreA').val(currentTeamAScore);
            $('#finalScoreB').val(currentTeamBScore);

            $('#winner-score').text(currentTeamAScore > currentTeamBScore ? currentTeamAScore : currentTeamBScore);


            if(currentTeamAScore==currentTeamBScore){
                if(currentTeamATime > currentTeamBTime)
                    $('#winner-team').text($('.scores .name').eq(1).text());
                else
                    $('#winner-team').text($('.scores .name').eq(0).text());
            }
            else {
                $('#winner-team').text(currentTeamAScore > currentTeamBScore ? $('.scores .name').eq(0).text() : $('.scores .name').eq(1).text());
            }

            vCenter();

            completeMatch();
        }

        $('#teamb .score-plus').hide();
        $('#teama .score-plus').hide();

        $('#level-monitor .que').text(currentQuestion+1);
    }

    function completeMatch(){
        $.ajax({
            type: "POST",
            url: baseUrl+'/complete-match',
            data: {
                schedule_id : $('#schedule').val(),
            },
            success: function(response){
            }
        });
    }

    function stopTimer(){
        $('#timer').removeClass('danger');
        clearInterval(timer);
        $('#timer').text('');
    }

    function stealTime(){
        startTimer(timerStealDuration);
        steal = true;

        $('#currentTeam').text("Oops, passing the question to "+$('.scores .name').eq((currentTurn === 0 ? 1 : 0)).text());
    }

    function checkAnswer(){
        selectedAnswer = $('#questions .wrap').eq(currentQuestion).find('button.active');

        if(selectedAnswer){

            if(selectedAnswer.hasClass('correct')){

                if(soundOn)
                    correctSound.play();

                selectedAnswer.addClass('answerCorrect');
                currentStep = 5;

                if(currentTurn===0){
                    steal ? $('#teamb .score-plus').text('+'+currentQ.attr('data-points')/2).show() : $('#teama .score-plus').text('+'+currentQ.attr('data-points')).show();
                    steal ? currentTeamBScore += parseInt(currentQ.attr('data-points'))/2 : currentTeamAScore += parseInt(currentQ.attr('data-points'));
                }
                else if(currentTurn===1){
                    steal ? $('#teama .score-plus').text('+'+currentQ.attr('data-points')/2).show() : $('#teamb .score-plus').text('+'+currentQ.attr('data-points')).show();
                    steal ? currentTeamAScore += parseInt(currentQ.attr('data-points'))/2 : currentTeamBScore += parseInt(currentQ.attr('data-points'));
                }

                if(currentTurn===0){
                    $('#currentTeam').text("That’s correct, "+(steal ?  $('.scores .name').eq(1).text() : $('.scores .name').eq(0).text())+"!");
                }
                else if(currentTurn===1){
                    $('#currentTeam').text("That’s correct, "+(steal ?  $('.scores .name').eq(0).text() : $('.scores .name').eq(1).text())+"!");
                }

                steal = false;

                updateScores();
                $('#timer').removeClass('bounce-top');
                stopTimer();

                $('#game').removeClass('danger').addClass('success');

            } else {

                $('#questions .wrap').eq(currentQuestion).find('.answers button').removeClass('active');

                $('#game').removeClass('success').addClass('danger');
                selectedAnswer.addClass('answerWrong');

                stopTimer();

                if(!steal){

                    if (soundOn)
                        wrongSound.play();

                    if($('#questions .wrap').eq(currentQuestion).find('button').length>2) {
                        stealTime();
                    } else {
                        $('#questions .wrap').eq(currentQuestion).find('button.correct').addClass('answerCorrect');
                        currentStep = 5;
                    }
                } else {

                    if(soundOn)
                        wrongSound.play();

                    $('#questions .wrap').eq(currentQuestion).find('button.correct').addClass('answerCorrect');
                    $('#currentTeam').text('Both teams are wrong ');

                    $('#timer').removeClass('bounce-top');
                    steal = false;
                    currentStep = 5;
                }
            }

        } else {
            stopTimer();
            if(!steal){

                if(soundOn)
                    wrongSound.play();

               if($('#questions .wrap').eq(currentQuestion).find('button').length>2) {
                    stealTime();
               } else {

                   $('#questions .wrap').eq(currentQuestion).find('button.correct').addClass('answerCorrect');
                   currentStep = 5;
               }
            } else {

                if(soundOn)
                    wrongSound.play();

                $('#questions .wrap').eq(currentQuestion).find('button.correct').addClass('answerCorrect');

                steal = false;
                currentStep = 5;
            }
        }

        updateButton();
    }

    function toggleTurns(){
        currentTurn = (currentTurn === 0 ? 1 : 0);
        updateTurn();
    }

    $(window).on('resize',function(){
        centerAlign();
        vCenter();
    });

    function addAnswerEvent(){
        $('.answers button').on('click',function(){
            if(timer) {
                $(this).closest('.answers').find('button').removeClass('active');
                if(!$(this).hasClass('active'))
                    $(this).addClass('active');
            } else {
                alert('Timer has not started yet!');
            }
        });
    }

    $('#categoryModal input').on('click',function(){
        $('#selectedCats').text($('#categoryModal input:checked').length);
    });

    $('#next').on('click',function(){

        if(currentStep != 21){
            currentQ = $('#questions .wrap').eq(currentQuestion);

            if(currentStep===1){
                currentQ.find('.question').show().addClass('rotate-in-center');
                currentStep++;
            }
            else if(currentStep===2){

                // if(currentQ.find('.answers .unshown').length)
                //     currentQ.find('.answers .unshown').first().removeClass('unshown').addClass('bounce-in-top');
                //
                // if(!currentQ.find('.answers .unshown').length)
                //     currentStep = 3;

                currentQ.find('.answers .unshown').first().removeClass('unshown');
                centerAlign();

                currentStep = 21;
                updateButton();

                $('#next').addClass('disabled');

                showAnswerLoop = setInterval(function(){

                    if(currentQ.find('.answers .unshown').length)
                        currentQ.find('.answers .unshown').first().removeClass('unshown');
                    else {
                        clearInterval(showAnswerLoop);
                        $('#next').removeClass('disabled');
                        currentStep = 4;
                        startTimer(timerDuration);
                    }

                    centerAlign();
                },settings['ad']);

            }
            else if(currentStep===3){
                startTimer(timerDuration);
                currentStep = 4;
            }
            else if(currentStep===4){
                if(currentQ.find('.answers button.active').length)
                    checkAnswer();
                else
                    alert('Please select an answer');
            }
            else if(currentStep===5){
                nextQuestion();
            }

            updateButton();
        }
    });


    $(window).on('load',function () {
        vCenter();

        setTimeout(function(){
            vCenter();
        },1000);
    })
</script>
<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID.
<script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set','transport','beacon'); ga('send', 'pageview')
</script>
<script src="https://www.google-analytics.com/analytics.js" async></script> -->

</body>

</html>
