<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{ asset('public/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/fonts.css') }}">

    <meta name="theme-color" content="#fafafa">
    <style>

        body {
            font-family: 'NeoSansStdRegular';
        }

        section {
            height: 100%;
            width: 100%;
            position: absolute;
            background-color: #c5dff6;
            display: none;
            background-image: url({{ asset('public/img/main-bg.jpg') }});
            background-position: center;
            background-repeat: repeat;
            background-size: auto 100%;
            overflow-x: hidden;
        }

        section.active {
            display: block;
        }

        .bounce-in-top{-webkit-animation:bounce-in-top .5s both;animation:bounce-in-top .5s both}
        @-webkit-keyframes bounce-in-top{0%{-webkit-transform:translateY(-500px);transform:translateY(-500px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in;opacity:0}38%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out;opacity:1}55%{-webkit-transform:translateY(-65px);transform:translateY(-65px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}72%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}81%{-webkit-transform:translateY(-28px);transform:translateY(-28px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}90%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}95%{-webkit-transform:translateY(-8px);transform:translateY(-8px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}100%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}}@keyframes bounce-in-top{0%{-webkit-transform:translateY(-500px);transform:translateY(-500px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in;opacity:0}38%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out;opacity:1}55%{-webkit-transform:translateY(-65px);transform:translateY(-65px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}72%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}81%{-webkit-transform:translateY(-28px);transform:translateY(-28px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}90%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}95%{-webkit-transform:translateY(-8px);transform:translateY(-8px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}100%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}}

        .slide-in-fwd-center{-webkit-animation:slide-in-fwd-center .4s cubic-bezier(.25,.46,.45,.94) both;animation:slide-in-fwd-center .4s cubic-bezier(.25,.46,.45,.94) both}
        @-webkit-keyframes slide-in-fwd-center{0%{-webkit-transform:translateZ(-1400px);transform:translateZ(-1400px);opacity:0}100%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1}}@keyframes slide-in-fwd-center{0%{-webkit-transform:translateZ(-1400px);transform:translateZ(-1400px);opacity:0}100%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1}}

        .rotate-in-center{-webkit-animation:rotate-in-center .6s cubic-bezier(.25,.46,.45,.94) both;animation:rotate-in-center .6s cubic-bezier(.25,.46,.45,.94) both}
        @-webkit-keyframes rotate-in-center{0%{-webkit-transform:rotate(-360deg);transform:rotate(-360deg);opacity:0}100%{-webkit-transform:rotate(0);transform:rotate(0);opacity:1}}@keyframes rotate-in-center{0%{-webkit-transform:rotate(-360deg);transform:rotate(-360deg);opacity:0}100%{-webkit-transform:rotate(0);transform:rotate(0);opacity:1}}

        .bounce-top {-webkit-animation:bounce-top 1s infinite both;animation:bounce-top 1s infinite both}
        @-webkit-keyframes bounce-top{0%{-webkit-transform:translateY(-45px);transform:translateY(-45px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in;opacity:1}24%{opacity:1}40%{-webkit-transform:translateY(-24px);transform:translateY(-24px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}65%{-webkit-transform:translateY(-12px);transform:translateY(-12px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}82%{-webkit-transform:translateY(-6px);transform:translateY(-6px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}93%{-webkit-transform:translateY(-4px);transform:translateY(-4px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}25%,55%,75%,87%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}100%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out;opacity:1}}@keyframes bounce-top{0%{-webkit-transform:translateY(-45px);transform:translateY(-45px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in;opacity:1}24%{opacity:1}40%{-webkit-transform:translateY(-24px);transform:translateY(-24px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}65%{-webkit-transform:translateY(-12px);transform:translateY(-12px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}82%{-webkit-transform:translateY(-6px);transform:translateY(-6px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}93%{-webkit-transform:translateY(-4px);transform:translateY(-4px);-webkit-animation-timing-function:ease-in;animation-timing-function:ease-in}25%,55%,75%,87%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out}100%{-webkit-transform:translateY(0);transform:translateY(0);-webkit-animation-timing-function:ease-out;animation-timing-function:ease-out;opacity:1}}

        .swing-in-top-fwd{-webkit-animation:swing-in-top-fwd .5s cubic-bezier(.175,.885,.32,1.275) both;animation:swing-in-top-fwd .5s cubic-bezier(.175,.885,.32,1.275) both}
        @-webkit-keyframes swing-in-top-fwd{0%{-webkit-transform:rotateX(-100deg);transform:rotateX(-100deg);-webkit-transform-origin:top;transform-origin:top;opacity:0}100%{-webkit-transform:rotateX(0deg);transform:rotateX(0deg);-webkit-transform-origin:top;transform-origin:top;opacity:1}}@keyframes swing-in-top-fwd{0%{-webkit-transform:rotateX(-100deg);transform:rotateX(-100deg);-webkit-transform-origin:top;transform-origin:top;opacity:0}100%{-webkit-transform:rotateX(0deg);transform:rotateX(0deg);-webkit-transform-origin:top;transform-origin:top;opacity:1}}

        .vCenter {
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -211px;
            margin-left: -211px;
        }

        .tble {
            background-color: rgba(0,0,0,0.5);
            border-radius: 20px;
            background-image: url({{ asset('public/img/line-bg.png') }});
            box-shadow: 0 0 20px #c9db2a;
            position: relative;
            min-height: 500px;
            float: left;
            display: block;
            width: 100%;
            padding-bottom: 30px;
        }

        .tble img {
            width: 100%;
            top: -290px;
            position: absolute;
        }

        .tble ul {
            padding: 0;
            margin: 0;
            margin-top: 150px;
        }

        .tble ul li {
            float: left;
            margin-bottom: 5px;
            text-align: center;
            color: #fff;
            list-style: none;
            width: 100%;
            padding: 0 30px;
            font-size: 14px;
        }


        .tble ul li .col-md-6  {
            width: 100%;
            height: 40px;
            line-height: 40px;
            background-image: url({{ asset('public/img/table-line.png') }});
            background-position: center;
            background-repeat: no-repeat;
        }



        .tble ul li .col-md-6.left  {
            width: 100%;
            height: 40px;
            line-height: 12px;
            background-image: url({{ asset('public/img/table-line.png') }});
            padding-top: 8px;
            background-position: center;
            background-repeat: no-repeat;
        }


        #bird {
            margin-left: -350px !important;
        }

        #scoreboard {
            padding: 100px 0;
        }

        @media only screen and (max-width: 991px) {
            .tble img {
                top: -120px;
            }
            #scoreboard {
                 padding: 50px 0;
            }
            .vCenter {
                top: 50% !important;
            }
        }

        @media only screen and (max-width: 767px) {
            #bird {
                display: none;
            }
            .tble img {
                top: -240px;
            }

            .vCenter {
                margin-left: 0 !important;
                left: 0 !important;
            }
        }


        @media only screen and (max-height: 960px) {
            section {
                background-size: 100% auto;
            }

            .vCenter {
                top: 60%;
            }
        }

        @media only screen and (max-height: 960px) and (max-width: 1440px) {
            section {
                background-size: auto 100%;
            }
        }

    </style>
</head>

<body>
<!--[if IE]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->
<section id="scoreboard" class="active">
    <div class="container vCenter">
        <div class="row">
            <div class="col-md-5"><img src="{{ asset('public/img/scorebird.png') }}" width="100%" id="bird" class="vCenter"></div>
            <div class="col-md-7">
                <div class="tble">
                    <img src="{{ asset('public/img/logo-ranking.png') }}">
                    <ul>
                        @foreach($teams as $index=>$team)
                        <li>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 left">{{$index+1}}. {{ $team->name }} - {{ $team->score }}pts <br/><span style="font-size: 10px;">(Time Consumed: {{$team->total_time}} seconds)</span></div><div class="col-md-6 col-sm-6 ">{{ $team->location->name }}</div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="{{ asset('public/js/vendor/modernizr-3.8.0.min.js') }}"></script>
<script src="{{ asset('public/js/vendor/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/js/plugins.js') }}"></script>
<script src="{{ asset('public/js/main.js') }}"></script>

<script>
    function vCenter(){
        $('.vCenter').each(function(){
            h = $(this).height();
            w = $(this).width();
            $(this).css('margin-top','-'+h/2+'px');
            $(this).css('margin-left','-'+w/2+'px');
        });
    }
    vCenter();

    $(window).on('resize',function(){
        setTimeout(function(){
            vCenter();
        },300);
        vCenter();
    });
</script>
<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID.
<script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set','transport','beacon'); ga('send', 'pageview')
</script>
<script src="https://www.google-analytics.com/analytics.js" async></script> -->

</body>

</html>
