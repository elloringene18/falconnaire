@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                @if(\Illuminate\Support\Facades\Session::has('message'))
                    <div class="col-md-12">
                        <div class="alert alert-danger">{{ \Illuminate\Support\Facades\Session::get('message') }}</div>
                    </div>
                @endif
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Add a question</h3>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/questions/store') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Question</label>
                                        <input type="text" class="form-control" name="value"  value="Sample Question">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Correct Answer</label><br/>
                                        <select class="form-control" name="correct-answer">
                                            <option value="0">Answer 1</option>
                                            <option value="1">Answer 2</option>
                                            <option value="2">Answer 3</option>
                                            <option value="3">Answer 4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Difficulty</label><br/>
                                        <select class="form-control" name="difficulty">
                                            <option value="0">Easy</option>
                                            <option value="1">Medium</option>
                                            <option value="2">Hard</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Category</label><br/>
                                        <select class="form-control" name="category_id">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{$category->name}}</option>
                                            @endforeach)
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <hr/>
                                        <p>
                                            <label for="exampleInputEmail3">Answer 1</label>
                                            <input type="text" class="form-control" name="answers[]"  value="test 1">
                                        </p>
                                        <p>
                                            <label for="exampleInputEmail3">Answer 2</label>
                                            <input type="text" class="form-control" name="answers[]"  value="test 2">
                                        </p>
                                        <p>
                                            <label for="exampleInputEmail3">Answer 3</label>
                                            <input type="text" class="form-control" name="answers[]"  value=" test 3">
                                        </p>
                                        <p>
                                            <label for="exampleInputEmail3">Answer 4</label>
                                            <input type="text" class="form-control" name="answers[]"  value=" test 4">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <a href="{{ url('/admin/questions') }}"><button type="button" class="btn btn-success mr-2">Cancel</button></a>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

@endsection
