@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                @if(\Illuminate\Support\Facades\Session::has('message'))
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ \Illuminate\Support\Facades\Session::get('message') }}</div>
                    </div>
                @endif
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Edit question</h3>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/questions/update') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                <input type="hidden" value="{{ $item->id }}" name="id">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Question</label>
                                        <input type="text" class="form-control" name="value"  value="{{ $item->value }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Correct Answer</label><br/>
                                        <?php
                                            $correct = 0;

                                            if(isset($item->answers[1]))
                                                if($item->answers[1]->is_correct)
                                                    $correct = 1;
                                        if(isset($item->answers[2]))
                                            if($item->answers[2]->is_correct)
                                                $correct = 2;
                                        if(isset($item->answers[3]))
                                            if($item->answers[3]->is_correct)
                                                $correct = 3;

                                        ?>

                                        <select class="form-control" name="correct-answer">
                                            <option value="0" {{ $correct == 0 ? 'selected' : ''}}>Answer 1</option>
                                            <option value="1" {{ $correct == 1 ? 'selected' : ''}}>Answer 2</option>
                                            <option value="2" {{ $correct == 2 ? 'selected' : ''}}>Answer 3</option>
                                            <option value="3" {{ $correct == 3 ? 'selected' : ''}}>Answer 4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Difficulty</label><br/>
                                        <select class="form-control" name="difficulty">
                                            <option value="0" {{ $item->difficulty == 0 ? 'selected' : ''}}>Easy</option>
                                            <option value="1" {{ $item->difficulty == 1 ? 'selected' : ''}}>Medium</option>
                                            <option value="2" {{ $item->difficulty == 2 ? 'selected' : ''}}>Hard</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Category</label><br/>
                                        <select class="form-control" name="category_id">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}" {{ $item->category_id == $category->id ? 'selected' : '' }}>{{$category->name}}</option>
                                            @endforeach)
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <hr/>
                                        <p>
                                            <label for="exampleInputEmail3">Answer 1</label>
                                            <input type="text" class="form-control" name="answers[]"  value="{{ isset($item->answers[0]) ? $item->answers[0]->value : '' }} ">
                                        </p>
                                        <p>
                                            <label for="exampleInputEmail3">Answer 2</label>
                                            <input type="text" class="form-control" name="answers[]"  value="{{ isset($item->answers[1]) ? $item->answers[1]->value : '' }}">
                                        </p>
                                        <p>
                                            <label for="exampleInputEmail3">Answer 3</label>
                                            <input type="text" class="form-control" name="answers[]"  value="{{ isset($item->answers[2]) ? $item->answers[2]->value : '' }}">
                                        </p>
                                        <p>
                                            <label for="exampleInputEmail3">Answer 4</label>
                                            <input type="text" class="form-control" name="answers[]"  value="{{ isset($item->answers[3]) ? $item->answers[3]->value : '' }}">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <a href="{{ url('/admin/questions') }}"><button type="button" class="btn btn-success mr-2">Back</button></a>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

@endsection
