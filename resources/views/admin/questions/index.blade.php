@extends('admin.partials.master')

@section('css')
    <style>
        .alert {
            margin-bottom: 3px;
            padding: 5px 10px;
        }
        body {
            font-size: 14px;
        }
    </style>
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">

                @if(\Illuminate\Support\Facades\Session::has('success'))
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ \Illuminate\Support\Facades\Session::get('success') }}</div>
                    </div>
                @endif

                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Questions
                                <a style="font-size: 14px;" href="{{ url('/admin/questions/create') }}" class="float-right">+ Add a question</a>
                            </h3>
                            <br/>
                            Category:
                            <select id="category">
                                @foreach($categories as $category)
                                    <option value="{{ $category->slug }}">{{$category->name}}</option>
                                @endforeach
                            </select>
                            &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;
                            Difficulty:
                            <select id="difficulty">
                                <option value="-1">All</option>
                                <option value="0">Easy</option>
                                <option value="1">Medium</option>
                                <option value="2">Hard</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @foreach($data as $item)
                                <div class="col-md-12 question" data-difficulty="{{$item->difficulty}}" data-category="{{$item->category->slug}}">
                                    <div class="row">
                                        <div class="col-md-10">

                                            {{ $item->value }}
                                            <br/><br/>
                                            @foreach($item->answers as $answer)
                                                <p class="alert {{ $answer->is_correct ? 'alert-success' : 'alert-danger' }} ">{{$answer->value}} </p>
                                            @endforeach
                                        </div>
                                        <div class="col-md-2">
                                            @if($item->difficulty==0)
                                                <div class="alert alert-primary">EASY</div>
                                            @elseif($item->difficulty==1)
                                                <div class="alert alert-info">MEDIUM</div>
                                            @elseif($item->difficulty==2)
                                                <div class="alert alert-danger">HARD</div>
                                            @endif
                                            <br/>
                                            <a href="{{ URL('admin/questions/'.$item->id) }}">View</a> |
                                            <a href="{{ URL('admin/questions/delete/'.$item->id) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>

                                        </div>
                                        <div class="col-md-12">
                                            <hr/>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>

        function filter(){
            $('.question').hide();

            $('.question[data-category="'+$('#category').val()+'"]').show();

            if($('#difficulty').val()>=0)
                $('.question[data-difficulty!="'+$('#difficulty').val()+'"]').hide();
        }
        filter();

        $('#category').on('change',function(){
            filter();
        });

        $('#difficulty').on('change',function(){
            filter();
        });

    </script>
@endsection
