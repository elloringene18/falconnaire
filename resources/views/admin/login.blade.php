<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{ asset('public/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/fonts.css') }}">

    <meta name="theme-color" content="#fafafa">
    <style>

        body {
            font-family: 'NeoSansStdRegular';
        }

        section {
            height: 100%;
            width: 100%;
            position: absolute;
            background-color: #c5dff6;
            background-image: url({{ asset('public/img/main-bg.jpg') }});
            background-position: center;
            background-repeat: repeat;
            background-size: auto 100%;
            overflow-x: hidden;
            display: block;
        }

        .vCenter {
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -211px;
            margin-left: -211px;
        }

        form input, form select {
            width: 100%;
            background: rgba(0,0,0,0.5);
            padding: 0 15px;
            color: #fff;
            border-radius: 10px;
            border-color: #cadb2b;
            font-size: 14px;
            height: 40px;
            line-height: 40px;
        }

        label {
            font-size: 14px;
            width: 100%;
            text-align: left;
        }

        form {
            background-image: url({{ asset('public/img/line-bg.png') }});
            background-color: rgba(0,0,0,0.5);
            border-radius: 20px;
            top: 51px;
            color: #fff;
            text-align: center;
            font-size: 20px;
            box-shadow: 1px 1px 20px #c9db2a;
            padding: 150px 30px 30px;
            margin-top: -100px;
            position: relative;
            z-index: 2;
        }

        #logo {
            position: absolute;
            left: 50%;
            top: -150px;
            margin-left: -300px;
        }

        .alert-success {
            font-size: 14px;
        }

        label span {
            float: left;
            display: block;
            height: 30px;
            line-height: 30px;
        }

        form input[type="submit"]{
            background-color: #7da830;
            color: #fff;
            border: 0;
            padding: 0;
            display: block;
            font-size: 18px;
            height: 40px;
            line-height: 40px;
        }

        .username {
            background-image: url('{{asset('public/img/user.png')}}');
            background-position: 95% center;
            background-repeat: no-repeat;
        }

        .password {
            background-image: url('{{asset('public/img/lock.png')}}');
            background-position: 95% center;
            background-repeat: no-repeat;
        }

        .alert {
            font-size: 12px;
        }

        input[type='checkbox'] {
            height: 14px;
            width: auto;
            margin: 0 0 0 5px;
            vertical-align: middle;
        }

        @media only screen and (max-width: 991px) {
            .tble img {
                top: -120px;
            }
            #scoreboard {
                padding: 50px 0;
            }
            .vCenter {
                top: 50% !important;
            }
        }

        @media only screen and (max-width: 767px) {
            #bird {
                display: none;
            }
            .tble img {
                top: -240px;
            }

            .vCenter {
                margin-left: 0 !important;
                left: 0 !important;
            }
        }


        @media only screen and (max-height: 960px) {
            section {
                background-size: 100% auto;
            }

            .vCenter {
                top: 60%;
            }
        }

        @media only screen and (max-height: 960px) and (max-width: 1440px) {
            section {
                background-size: auto 100%;
            }
        }

        #bird {
            position: absolute;
            bottom: 0;
            right: 0;
            z-index: 1;
        }

        #bird {
            position: absolute;
            bottom: 0;
            right: 0;
            z-index: 1;
        }

    </style>
</head>

<body>
<!--[if IE]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->
<section id="scoreboard" class="active">
    <div class="container vCenter">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <form class="forms-sample" action="{{ url('/login') }}" method="post" enctype="multipart/form-data">
                            <img src="{{ asset('public/img/logo-glow.png') }}" id="logo">
                            <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                            @if(\Illuminate\Support\Facades\Session::has('success'))
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="alert alert-success">{{ \Illuminate\Support\Facades\Session::get('success') }}</div>
                                    </div>
                                </div>
                            @endif

                            @if(\Illuminate\Support\Facades\Session::has('error'))
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="alert alert-danger">{{ \Illuminate\Support\Facades\Session::get('error') }}</div>
                                    </div>
                                </div>
                            @endif

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail3">Username</label>
                                    <input type="text" class="username" name="email"  placeholder="Username" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail3">Password</label>
                                    <input type="password" class="password" name="password"  placeholder="Password" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><span>Remember me :</span><span><input type="checkbox" name="remember"></span></label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="submit" value="Sign in">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img src="{{ asset('public/img/login-bird.png') }}" id="bird">
</section>

<script src="{{ asset('public/js/vendor/modernizr-3.8.0.min.js') }}"></script>
<script src="{{ asset('public/js/vendor/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/js/plugins.js') }}"></script>
<script src="{{ asset('public/js/main.js') }}"></script>

<script>
    function vCenter(){
        $('.vCenter').each(function(){
            h = $(this).height();
            w = $(this).width();
            $(this).css('margin-top','-'+h/2+'px');
            $(this).css('margin-left','-'+w/2+'px');
        });
    }
    vCenter();

    $(window).on('resize',function(){
        setTimeout(function(){
            vCenter();
        },300);
    });

    $(window).on('load',function () {
        setTimeout(function(){
            $('.alert').hide();
        },10000);
        vCenter();
    })
</script>
<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID.
<script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('set','transport','beacon'); ga('send', 'pageview')
</script>
<script src="https://www.google-analytics.com/analytics.js" async></script> -->

</body>

</html>
