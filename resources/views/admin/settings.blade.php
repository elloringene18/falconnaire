@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                @if(\Illuminate\Support\Facades\Session::has('success'))
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ \Illuminate\Support\Facades\Session::get('success') }}</div>
                    </div>
                @endif
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Settings</h3>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/settings/update') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Answers Appearance Speed</label>
                                        <select class="form-control" name="settings[answer-duration]">
                                            <option value="300" {{ $settings['answer-duration'] == '300' ? 'selected' : '' }}>.3 Seconds</option>
                                            <option value="500" {{ $settings['answer-duration'] == '500' ? 'selected' : '' }}>.5 Seconds</option>
                                            <option value="1000" {{ $settings['answer-duration'] == '1000' ? 'selected' : '' }}>1 Second</option>
                                            <option value="2000" {{ $settings['answer-duration'] == '2000' ? 'selected' : '' }}>2 Seconds</option>
                                            <option value="3000" {{ $settings['answer-duration'] == '3000' ? 'selected' : '' }}>3 Seconds</option>
                                            <option value="4000" {{ $settings['answer-duration'] == '4000' ? 'selected' : '' }}>4 Seconds</option>
                                            <option value="5000" {{ $settings['answer-duration'] == '5000' ? 'selected' : '' }}>5 Seconds</option>
                                            <option value="6000" {{ $settings['answer-duration'] == '6000' ? 'selected' : '' }}>6 Seconds</option>
                                            <option value="7000" {{ $settings['answer-duration'] == '7000' ? 'selected' : '' }}>7 Seconds</option>
                                            <option value="8000" {{ $settings['answer-duration'] == '8000' ? 'selected' : '' }}>8 Seconds</option>
                                            <option value="9000" {{ $settings['answer-duration'] == '9000' ? 'selected' : '' }}>9 Seconds</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Easy Question Points</label>
                                        <select class="form-control" name="settings[easy-points]">
                                            <option value="1" {{ $settings['easy-points'] == '1' ? 'selected' : '' }}>1 Point</option>
                                            <option value="2" {{ $settings['easy-points'] == '2' ? 'selected' : '' }}>2 Points</option>
                                            <option value="3" {{ $settings['easy-points'] == '3' ? 'selected' : '' }}>3 Points</option>
                                            <option value="4" {{ $settings['easy-points'] == '4' ? 'selected' : '' }}>4 Points</option>
                                            <option value="5" {{ $settings['easy-points'] == '5' ? 'selected' : '' }}>5 Points</option>
                                            <option value="10" {{ $settings['easy-points'] == '10' ? 'selected' : '' }}>10 Points</option>
                                            <option value="15" {{ $settings['easy-points'] == '15' ? 'selected' : '' }}>15 Points</option>
                                            <option value="20" {{ $settings['easy-points'] == '20' ? 'selected' : '' }}>20 Points</option>
                                            <option value="30" {{ $settings['easy-points'] == '30' ? 'selected' : '' }}>30 Points</option>
                                            <option value="50" {{ $settings['easy-points'] == '50' ? 'selected' : '' }}>50 Points</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Medium Question Points</label>
                                        <select class="form-control" name="settings[medium-points]">
                                            <option value="1" {{ $settings['medium-points'] == '1' ? 'selected' : '' }}>1 Point</option>
                                            <option value="2"{{ $settings['medium-points'] == '2' ? 'selected' : '' }}>2 Points</option>
                                            <option value="3"{{ $settings['medium-points'] == '3' ? 'selected' : '' }}>3 Points</option>
                                            <option value="4"{{ $settings['medium-points'] == '4' ? 'selected' : '' }}>4 Points</option>
                                            <option value="5"{{ $settings['medium-points'] == '5' ? 'selected' : '' }}>5 Points</option>
                                            <option value="10"{{ $settings['medium-points'] == '10' ? 'selected' : '' }}>10 Points</option>
                                            <option value="15"{{ $settings['medium-points'] == '15' ? 'selected' : '' }}>15 Points</option>
                                            <option value="20"{{ $settings['medium-points'] == '20' ? 'selected' : '' }}>20 Points</option>
                                            <option value="30"{{ $settings['medium-points'] == '30' ? 'selected' : '' }}>30 Points</option>
                                            <option value="50"{{ $settings['medium-points'] == '50' ? 'selected' : '' }}>50 Points</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Hard Question Points</label>
                                        <select class="form-control" name="settings[hard-points]">
                                            <option value="1" {{ $settings['hard-points'] == '1' ? 'selected' : '' }}>1 Point</option>
                                            <option value="2" {{ $settings['hard-points'] == '2' ? 'selected' : '' }}>2 Points</option>
                                            <option value="3" {{ $settings['hard-points'] == '3' ? 'selected' : '' }}>3 Points</option>
                                            <option value="4" {{ $settings['hard-points'] == '4' ? 'selected' : '' }}>4 Points</option>
                                            <option value="5" {{ $settings['hard-points'] == '5' ? 'selected' : '' }}>5 Points</option>
                                            <option value="10" {{ $settings['hard-points'] == '10' ? 'selected' : '' }}>10 Points</option>
                                            <option value="15" {{ $settings['hard-points'] == '15' ? 'selected' : '' }}>15 Points</option>
                                            <option value="20" {{ $settings['hard-points'] == '20' ? 'selected' : '' }}>20 Points</option>
                                            <option value="30" {{ $settings['hard-points'] == '30' ? 'selected' : '' }}>30 Points</option>
                                            <option value="50" {{ $settings['hard-points'] == '50' ? 'selected' : '' }}>50 Points</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Timer Duration</label>
                                        <select class="form-control" name="settings[timer]">
                                            <option value="10" {{ $settings['timer'] == '10' ? 'selected' : '' }}>10 Seconds</option>
                                            <option value="15" {{ $settings['timer'] == '15' ? 'selected' : '' }}>15 Seconds</option>
                                            <option value="20" {{ $settings['timer'] == '20' ? 'selected' : '' }}>20 Seconds</option>
                                            <option value="30" {{ $settings['timer'] == '30' ? 'selected' : '' }}>30 Seconds</option>
                                            <option value="40" {{ $settings['timer'] == '40' ? 'selected' : '' }}>40 Seconds</option>
                                            <option value="50" {{ $settings['timer'] == '50' ? 'selected' : '' }}>50 Seconds</option>
                                            <option value="60" {{ $settings['timer'] == '60' ? 'selected' : '' }}>60 Seconds</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Steal Timer Duration ( When an opposing team makes a wrong answer )</label>
                                        <select class="form-control" name="settings[steal-timer]">
                                            <option value="10" {{ $settings['steal-timer'] == '10' ? 'selected' : '' }}>10 Seconds</option>
                                            <option value="15" {{ $settings['steal-timer'] == '15' ? 'selected' : '' }}>15 Seconds</option>
                                            <option value="20" {{ $settings['steal-timer'] == '20' ? 'selected' : '' }}>20 Seconds</option>
                                            <option value="30" {{ $settings['steal-timer'] == '30' ? 'selected' : '' }}>30 Seconds</option>
                                            <option value="40" {{ $settings['steal-timer'] == '40' ? 'selected' : '' }}>40 Seconds</option>
                                            <option value="50" {{ $settings['steal-timer'] == '50' ? 'selected' : '' }}>50 Seconds</option>
                                            <option value="60" {{ $settings['steal-timer'] == '60' ? 'selected' : '' }}>60 Seconds</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <a href="{{ url('/admin/teams') }}"><button type="button" class="btn btn-success mr-2">Back</button></a>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

@endsection
