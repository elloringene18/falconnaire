@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                @if(\Illuminate\Support\Facades\Session::has('success'))
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ \Illuminate\Support\Facades\Session::get('success') }}</div>
                    </div>
                @endif
                @if(\Illuminate\Support\Facades\Session::has('error'))
                    <div class="col-md-12">
                        <div class="alert alert-danger">{{ \Illuminate\Support\Facades\Session::get('error') }}</div>
                    </div>
                @endif

            </div>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body row">
                            <div class="col-md-12">
                                Edit Match: {{ $schedule->date_start->format('M d (H:i') }} - {{ $schedule->date_end->format('  H:i)') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <form class="forms-sample" action="{{ url('admin/matches/update') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                <input type="hidden" value="{{ $schedule->id }}" name="id">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Team A</label>
                                        <select name="team_a_id" class="form-control">
                                            <option value="null">Not Set</option>
                                            @if($schedule->team_a_id)
                                                @foreach($teams as $team)
                                                    <option {{ $team->id == $schedule->team_a_id ? 'selected' : '' }} value="{{ $team->id }}">{{$team->name}}</option>
                                                @endforeach
                                            @else
                                                @foreach($teams as $team)
                                                    <option value="{{ $team->id }}">{{$team->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Team B</label>
                                        <select name="team_b_id" class="form-control">
                                            <option value="null">Not Set</option>
                                            @if($schedule->team_b_id)
                                                @foreach($teams as $team)
                                                    <option {{ $team->id == $schedule->team_b_id ? 'selected' : '' }} value="{{ $team->id }}">{{$team->name}}</option>
                                                @endforeach
                                            @else
                                                @foreach($teams as $team)
                                                    <option value="{{ $team->id }}">{{$team->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <a href="{{ url('/admin/matches') }}"><button type="button" class="btn btn-success mr-2">Cancel</button></a>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

@endsection
