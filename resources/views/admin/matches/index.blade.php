@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Matches</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="dataTable">
                                    <tr>
                                        <th onclick="sortTable(3)">Schedule</th>
                                        <th onclick="sortTable(0)">Team 1</th>
                                        <th onclick="sortTable(1)">Team 2</th>
                                        <th onclick="sortTable(2)">Winner</th>
                                        <th onclick="sortTable(4)">Played</th>
                                        <th onclick="sortTable(5)"></th>
                                    </tr>
                                    @foreach($data as $item)
                                        <tr>
                                            <td>{{ $item->date_start->format('M d H:i') }}</td>
                                            <td>{{ $item->teama ? $item->teama->name : 'Not set' }} ({{ $item->teama ? $item->teama->score : '0' }}Pts)</td>
                                            <td>{{ $item->teamb ? $item->teamb->name : 'Not set' }} ({{ $item->teamb ? $item->teamb->score : '0' }}Pts)</td>

                                            @if($item->date_ended && $item->teamb && $item->teama)
                                                @if($item->teama->score == $item->teamb->score)
                                                    <td>{{ $item->teama->total_time < $item->teamb->total_time ? $item->teama->name : $item->teamb->name }}</td>
                                                @else
                                                    <td>{{ $item->teama->score > $item->teamb->score ? $item->teama->name : $item->teamb->name }}</td>
                                                @endif
                                            @else
                                                <td>N/A</td>
                                            @endif

                                            <td>{{ $item->date_ended ? 'Yes' : 'No' }}</td>
                                            <td>
                                                <a href="{{ URL('admin/matches/edit/'.$item->id) }}" >Edit</a> |
                                                <a href="{{ URL('admin/matches/delete/'.$item->id) }}" onclick="return confirm('Are you sure you want to delete this item? Match will be reset and timeslot will remain.');">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                                <br/>
                                <br/>
                                <p style="font-size: 10px;">Deleting a match will allow the teams included to play again.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
