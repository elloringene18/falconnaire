@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                @if(\Illuminate\Support\Facades\Session::has('success'))
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ \Illuminate\Support\Facades\Session::get('success') }}</div>
                    </div>
                @endif
                @if(\Illuminate\Support\Facades\Session::has('error'))
                    <div class="col-md-12">
                        <div class="alert alert-danger">{{ \Illuminate\Support\Facades\Session::get('error') }}</div>
                    </div>
                @endif

                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Add a Location</h3>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/locations/store') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Location Name</label>
                                        <input type="text" class="form-control" name="name"  placeholder="Location Name">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <a href="{{ url('/admin/locations') }}"><button type="button" class="btn btn-success mr-2">Cancel</button></a>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

@endsection
