@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                @if(\Illuminate\Support\Facades\Session::has('error'))
                    <div class="col-md-12">
                        <div class="alert alert-danger">{{ \Illuminate\Support\Facades\Session::get('error') }}</div>
                    </div>
                @endif
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Add a Team</h3>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/teams/store') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Team Name</label>
                                        <input type="text" class="form-control" name="name"  value="{{ isset($_GET['name']) ? $_GET['name'] : 'Team Name' }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="exampleInputEmail3">Location</label><br/>
                                            <select class="form-control" name="location_id">
                                                @foreach($locations as $location)
                                                    <option value="{{$location->id}}" {{ isset($_GET['location_id']) ? ( $_GET['location_id'] == $location->id ? 'selected' : false ) : '' }}>{{ $location->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <hr/>
                                        Members
                                        <br/>
                                        <br/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 1 Name:</label>
                                                <input type="text" class="form-control" name="members[0][name]"  value="{{ isset($_GET['member_name0']) ? $_GET['member_name0'] : 'name 1' }}">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 1 FID:</label>
                                                <input type="text" class="form-control" name="members[0][id]"  value="{{ isset($_GET['member_id0']) ? $_GET['member_id0'] : 'ID00001' }}">
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 2 Name:</label>
                                                <input type="text" class="form-control" name="members[1][name]"  value="{{ isset($_GET['member_name1']) ? $_GET['member_name1'] : 'name 2' }}">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 2 FID:</label>
                                                <input type="text" class="form-control" name="members[1][id]"  value="{{ isset($_GET['member_id1']) ? $_GET['member_id1'] : 'ID00002' }}">
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 3 Name:</label>
                                                <input type="text" class="form-control" name="members[2][name]"  value="{{ isset($_GET['member_name2']) ? $_GET['member_name2'] : 'name 3' }}">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 3 FID:</label>
                                                <input type="text" class="form-control" name="members[2][id]"  value="{{ isset($_GET['member_id2']) ? $_GET['member_id2'] : 'ID00003' }}">
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 4 Name:</label>
                                                <input type="text" class="form-control" name="members[3][name]"  value="{{ isset($_GET['member_name3']) ? $_GET['member_name3'] : 'name 4' }}">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 4 FID:</label>
                                                <input type="text" class="form-control" name="members[3][id]"  value="{{ isset($_GET['member_id3']) ? $_GET['member_id3'] : 'ID00003' }}">
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 5 Name:</label>
                                                <input type="text" class="form-control" name="members[4][name]"  value="{{ isset($_GET['member_name4']) ? $_GET['member_name4'] : 'name 5' }}">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 5 FID:</label>
                                                <input type="text" class="form-control" name="members[4][id]"  value="{{ isset($_GET['member_id4']) ? $_GET['member_id4'] : 'ID00005' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <a href="{{ url('/admin/questions') }}"><button type="button" class="btn btn-success mr-2">Cancel</button></a>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

@endsection
