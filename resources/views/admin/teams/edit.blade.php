@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                @if(\Illuminate\Support\Facades\Session::has('message'))
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ \Illuminate\Support\Facades\Session::get('message') }}</div>
                    </div>
                @endif
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Add a Team</h3>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/teams/update') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{{ $item->id }}" name="id">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Team Name</label>
                                        <input type="text" class="form-control" name="name"  value="{{ $item->name }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="exampleInputEmail3">Location</label><br/>
                                            <select class="form-control" name="location_id">
                                                @foreach($locations as $location)
                                                    <option value="{{$location->id}}" {{ $item->location ? ($item->location->id == $location->id ? 'selected' : '') : ''}}>{{ $location->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <hr/>
                                        Members
                                        <br/>
                                        <br/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 1 Name:</label>
                                                <input type="text" class="form-control" name="members[0][name]"  value="{{ isset($item->members[0]) ? $item->members[0]->name : '' }}">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 1 FID:</label>
                                                <input type="text" class="form-control" name="members[0][id]"  value="{{ isset($item->members[0]) ? $item->members[0]->fid : '' }}">
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 2 Name:</label>
                                                <input type="text" class="form-control" name="members[1][name]"  value="{{ isset($item->members[1]) ? $item->members[1]->name : '' }}">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 2 FID:</label>
                                                <input type="text" class="form-control" name="members[1][id]"  value="{{ isset($item->members[1]) ? $item->members[1]->fid : '' }}">
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 3 Name:</label>
                                                <input type="text" class="form-control" name="members[2][name]"  value="{{ isset($item->members[2]) ? $item->members[2]->name : '' }}">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 3 FID:</label>
                                                <input type="text" class="form-control" name="members[2][id]"  value="{{ isset($item->members[2]) ? $item->members[2]->fid : '' }}">
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 4 Name:</label>
                                                <input type="text" class="form-control" name="members[3][name]"  value="{{ isset($item->members[3]) ? $item->members[3]->name : '' }}">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 4 FID:</label>
                                                <input type="text" class="form-control" name="members[3][id]"  value="{{ isset($item->members[3]) ? $item->members[3]->fid : '' }}">
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 5 Name:</label>
                                                <input type="text" class="form-control" name="members[4][name]"  value="{{ isset($item->members[4]) ? $item->members[4]->name : ''}}">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="exampleInputEmail3">Member 5 FID:</label>
                                                <input type="text" class="form-control" name="members[4][id]"  value="{{ isset($item->members[4]) ? $item->members[4]->fid : '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <a href="{{ url('/admin/teams') }}"><button type="button" class="btn btn-success mr-2">Back</button></a>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

@endsection
