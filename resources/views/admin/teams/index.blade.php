@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            @if(\Illuminate\Support\Facades\Session::has('success'))
                <div class="col-md-12">
                    <div class="alert alert-success">{{ \Illuminate\Support\Facades\Session::get('success') }}</div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h3>Teams</h3>
                            <br/>
                            <a href="{{ url('/admin/teams/create') }}">+ Add a team</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="dataTable">
                                    <tr>
                                        <th onclick="sortTable(0)">Name</th>
                                        <th onclick="sortTable(1)">Location</th>
                                        <th onclick="sortTable(2)">Score</th>
                                        <th onclick="sortTable(3)">Action</th>
                                    </tr>
                                    @foreach($data as $item)
                                        <tr>
                                            <td>
                                                <h5>{{ $item->name }}</h5><br/>
                                                <ul>
                                                    @foreach($item->members as $member)
                                                            <li>{{ $member->name }} ({{ $member->fid }})</li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                            <td>{{ $item->location ? $item->location->name : 'N/A' }}</td>
                                            <td>{{ $item->score }}</td>
                                            <td>
                                                <a href="{{ URL('admin/teams/'.$item->id) }}">View</a> |
                                                <a href="{{ URL('admin/teams/delete/'.$item->id) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a></td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
