<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#teams" aria-expanded="false" aria-controls="forms">
                <i class="menu-icon mdi mdi-human-male-female"></i>
                <span class="menu-title">Teams</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="teams">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/teams') }}">View All</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/teams/create') }}">Create</a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#locations" aria-expanded="false" aria-controls="forms">
                <i class="menu-icon mdi mdi-map-marker-circle"></i>
                <span class="menu-title">Locations</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="locations">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/locations') }}">View All</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/locations/create') }}">Create</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#categories" aria-expanded="false" aria-controls="forms">
                <i class="menu-icon mdi mdi-format-list-bulleted"></i>
                <span class="menu-title">Categories</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="categories">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/categories') }}">View All</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/categories/create') }}">Create</a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#questions" aria-expanded="false" aria-controls="forms">
                <i class="menu-icon mdi mdi-help-circle-outline"></i>
                <span class="menu-title">Questions</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="questions">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/questions') }}">View All</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/questions/create') }}">Create</a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ URL('admin/matches') }}" aria-expanded="false" aria-controls="forms">
                <i class="menu-icon mdi mdi-chart-areaspline"></i>
                <span class="menu-title">Matches</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ URL('scoreboard') }}" target="_blank" aria-expanded="false" aria-controls="forms">
                <i class="menu-icon mdi mdi-format-list-bulleted"></i>
                <span class="menu-title">Scoreboard</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ URL('admin/settings') }}" aria-expanded="false" aria-controls="forms">
                <i class="menu-icon mdi mdi-star-outline"></i>
                <span class="menu-title">Settings</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ URL('game') }}" target="_blank"  aria-expanded="false" aria-controls="forms">
                <i class="menu-icon mdi mdi-sword-cross"></i>
                <span class="menu-title">Start a Match</span>
            </a>
        </li>
    </ul>
</nav>
