@extends('admin.partials.master')

@section('css')
    <style>
        p {
            font-weight: 500;
        }

        strong {
            color: #c10c0c;
        }
    </style>
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
            @if(\Illuminate\Support\Facades\Session::has('success'))
                <div class="col-md-12">
                    <div class="alert alert-success">{{ \Illuminate\Support\Facades\Session::get('success') }}</div>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Who wants to be a Falconaire</h3>
                                    <h4>Admin Secton</h4>
                                    <br/>
                                    <p>Navigate the admin section through the links on the left side.</p>
                                    <br/>
                                </div>
                                <div class="col-md-6">
                                    <h5>Under <strong>Teams</strong>, you can do the below:</h5>
                                    <ul>
                                        <li>View All Teams</li>
                                        <li>Add a Teams</li>
                                        <li>Edit a Teams</li>
                                        <li>Delete a Teams</li>
                                    </ul>

                                    <br/>
                                    <h5>Under <strong>Locations</strong>, you can do the below:</h5>
                                    <ul>
                                        <li>View All Locations</li>
                                        <li>Add a Location</li>
                                        <li>Edit a Location</li>
                                        <li>Delete a Location</li>
                                    </ul>

                                    <br/>
                                    <h5>Under <strong>Questions</strong>, you can do the below:</h5>
                                    <ul>
                                        <li>View All Questions</li>
                                        <li>Add a Question and corresponding answers</li>
                                        <li>Edit a Question and corresponding answers</li>
                                        <li>Delete a Question and corresponding answers</li>
                                    </ul>

                                </div>
                                <div class="col-md-6">
                                    <h5>Under <strong>Matches</strong>, you can:</h5>
                                    <ul>
                                        <li>View on-goind and completed matches</li>
                                        <li>Delete a Match (Lets the teams play again.</li>
                                    </ul>
                                    <br/>
                                    <h5>Under <strong>Settings</strong>, you can adjust the below:</h5>
                                    <ul>
                                        <li>Change answer appearance speed</li>
                                        <li>Points for easy questions</li>
                                        <li>Points for medium questions</li>
                                        <li>Points for hard questions</li>
                                        <li>Timer Duration</li>
                                        <li>Steal Timer Duration</li>
                                    </ul>
                                    <br/>

                                    <h5>Start the game by clicking on  <strong>Start a Match</strong><br/>or by visiting this link <a href="{{ url('/game') }}">{{ url('/game') }}<a/></p>

                                </div>
                            </div>
                            <br/>
                            <br/>

                            <div class="row">
                                <div class="col-md-12">
                                    <p style="font-size: 10px;">If you have any questions, please email us at <a href="mailto:gene@thisishatch.com">gene@thisishatch.com</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
