<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/game');
});

Route::get('/questions/create', function () {
    return view('admin.forms.create');
});

Route::get('/scoreboard','GameController@scoreboard');
Route::get('/get-teams-by-location/{location_id}','TeamController@getByLocation');
Route::get('/get-teams-by-schedule/{schedule_id}','TeamController@getBySchedule');
Route::get('/check-team-matches/{team_id1}/{team_id2}','GameController@checkTeamMatch');
Route::post('/update-team-scores','GameController@updateTeamScores');
Route::get('/register','TeamController@register');
Route::post('/register-team','TeamController@registerTeam');
Route::post('/complete-match','GameController@completeMatch');

Route::get('/login', function () {
    return view('admin.login');
})->name('login');

Route::get('/logout', function () {
    \Illuminate\Support\Facades\Auth::logout();
    return view('admin.login');
})->name('login');

Route::post('/login', 'Auth\LoginController@authenticate');

Route::get('/game', 'GameController@index')->middleware('auth');
Route::post('/get-questions-by-cat', 'GameController@getQuestionsByCat');

Route::group(['prefix'=>'admin','as'=>'admin.','middleware'=>'auth'],function () {

    Route::get('/', function () {
        return view('admin.welcome');
    });

    Route::group(['prefix'=>'teams','as'=>'teams.'],function () {
        Route::get('/','TeamController@index');
        Route::get('/create','TeamController@create');
        Route::post('/store','TeamController@store');
        Route::get('/{id}','TeamController@edit');
        Route::post('/update','TeamController@update');
        Route::get('/delete/{id}','TeamController@delete');
        Route::get('/get-by-location/{location_id}','TeamController@getByLocation');
    });

    Route::group(['prefix'=>'locations','as'=>'locations.'],function () {
        Route::get('/','LocationController@index');
        Route::get('/create','LocationController@create');
        Route::post('/store','LocationController@store');
        Route::get('/{id}','LocationController@edit');
        Route::post('/update','LocationController@update');
        Route::get('/delete/{id}','LocationController@delete');
    });

    Route::group(['prefix'=>'categories','as'=>'categories.'],function () {
        Route::get('/','CategoryController@index');
        Route::get('/create','CategoryController@create');
        Route::post('/store','CategoryController@store');
        Route::get('/{id}','CategoryController@edit');
        Route::post('/update','CategoryController@update');
        Route::get('/delete/{id}','CategoryController@delete');
    });

    Route::group(['prefix'=>'matches','as'=>'matches.'],function () {
        Route::get('/','MatchController@index');
        Route::get('/delete/{id}','MatchController@delete');
        Route::get('/edit/{id}','MatchController@edit');
        Route::post('/update/','MatchController@update');
    });

    Route::group(['prefix'=>'questions','as'=>'questions.'],function () {
        Route::get('/','QuestionController@index');
        Route::get('/create','QuestionController@create');
        Route::post('/store','QuestionController@store');
        Route::get('/{id}','QuestionController@edit');
        Route::post('/update','QuestionController@update');
        Route::get('/delete/{id}','QuestionController@delete');
    });

    Route::group(['prefix'=>'settings','as'=>'settings.'],function () {
        Route::get('/','GameController@settings');
        Route::post('/update','GameController@settingsUpdate');
    });

});

